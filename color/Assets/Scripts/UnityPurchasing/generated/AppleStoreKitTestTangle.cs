// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("0QkhkGI7u6RS+0O+UsSF76RACUGwHNIcbZebm5GfmqrFq4uqlZyZz5Gj1lHV9FopxS0PICMyx9DuMXX1ye716P/R8+6qhI2XqKqqrqqrq62qkJySsZybn5+dmZmql5yTsBzSHB/i0lRaX4iL8JWWNLWfUPXg/uW6+uytyHZlNoPHlo+1EJVvE1JNsYdYYXzFiFuYmZuamzmhqqOqlZyZz7iql5yTsBzSHG2Xm5ubn5qZGJuV7quLqpWcmc+ekJaSye716P/R8+6HiZubZZ6fqpmbm2WqlJyZz4eVmxZ760HQ32m3gwyZzBs83nTv7cHJSeyKaGrzdlf4A+tj+hAe0qJeJxGvqKmvwI2Xrqqqqaitq62vqKmvwBE2bwUqk6I59OFRvMqQgHsTS+jsq4ALGiMlf5ZRpiGU3vi8YJ6zLNORqpOcmc+enImYz8mrjaqPnJOwHP2/IzH2qf83QlgA4tKVc1ZbvZdcWt2y0XHAW7GTzLY/MJOZ4airRnJV/qb3GsOpFlCsnLCLgRfTVJJl/KoYme6qGJjGOpmYm5iYm5uql5yTlyGiCvGcXmaXfw/KVwtSIL9uwlPbP+STFyCnp1fqzUJ3dU9A7VIJnvkxDQCxWbBCSih7T8CXo4H1/bIztu981iuISWd5hcSX5yLCTTrmfuWGdZMS4v9UG2gX6H2dgPS8iG2oK9IcbZebk5uMksnu9ej/0fPuqhibqsWri6qVnJnPnpmWksnu9ej/0fOYz8mrjaqPnJOwHNIcbZebk5uMknEt2hunHafqy0texxc/RuXwwoBElpLJ7vXo/9Hz7quRqpOcmc+enImri6qVnJnPnpGWksnu9ej/0fPuq5tlnp6ZmJgeqoycmc+Hv5ubZZ6WA2EpEM8FdYeyIsWwS+kFr92tIdxeM5EpVfiVYS4mbUEexLJx3ZdrdydaTIfaBM0jogrt7MhIMtHq9CL9bZebm5GfmpkYm5uaKJp6pmtyc/iaqhibkJgYm5uaQeUKprbplUENvFXiNscgpMXKscazFkdYTbCNRbgukJaSye716P/R8+6ri6qVnJnPnpGemZaSye716P/R8+6ri6qVnJnPnogZTB3i9wMnxCmQegJUbHBZFPfzffV6gXyX8le/9Fnskm/QkTTg7qZNfuIyNTL3PVtA8rf8VlWzkcq2eYR2sQufPafV");
        private static int[] order = new int[] { 15,1,20,6,30,38,43,27,13,9,28,24,34,27,19,16,24,35,37,36,24,23,23,40,39,40,34,27,38,42,30,38,42,33,37,42,38,38,40,43,43,41,42,43,44 };
        private static int key = 154;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
