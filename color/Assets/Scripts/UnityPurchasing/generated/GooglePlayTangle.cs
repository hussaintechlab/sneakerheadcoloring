// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("3gy1CkVb0GEpq+4kOfCM5qmfQY4mEcSnT7+gziakgmLVgEdzxdm0Y9eMkrM+9uwDBPZ90mRbMlVojc/0zZhp9I/nPolXsLr69IBxlLblAMMpImWcaKlCLKvZ+8D/3Ojr+Vn14Fm+wrTMb6YslKo1I9csyz49jTdl6AJbmTQKAwmxJn6/eytq7HVyoGYNVKRUc/44WYYZyQBE4WLPI6HpMk26Cd7G/XjrLgfinQGjoteYAGDNrF8JMjfWIujx1VVUcXt7vXb0NVLMfv3ezPH69dZ6tHoL8f39/fn8/3798/zMfv32/n79/fx6AkN+GAKFwGXJritGKiQQjKG4jzwbjNM1e8WrscDqdB4OCsqziSyG0voMMhpCMEKhPEwpwugb4f7//fz9");
        private static int[] order = new int[] { 12,7,6,11,6,10,9,13,9,10,12,13,12,13,14 };
        private static int key = 252;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
