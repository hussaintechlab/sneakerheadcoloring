// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("6OAy0QQQFoLsbALwu7igM46l4A/rnz1hdolmlppMlSiX4WdgUrTi735lJGPJcCm0TsGMnajgbLoQKRgnyFrKnboIL7ZE6GFzQatbfbsTSpBORUppxQvFtE5CQkZGQ0DBQkJDHzcrLDEqNzpyVXNXRUAWR0BQTgIzw1dokyoE1zVKvbcozm0D5bQEDjxFc0xFQBZeUEJCvEdGc0BCQrxzXmMiLSdjICYxNyolKiAiNyosLWMz1t05T+cEyBiXVXRwiIdMDo1XKpJlc2dFQBZHSFBeAjMzLyZjACYxN0zefrBoCmtZi72N9vpNmh1flYh+dnFyd3NwdRlUTnB2c3FzenFyd3NFQBZeTUdVR1dokyoE1zVKvbcozjcqJSogIjcmYyE6YyItOmMzIjE39Fj+0AFnUWmETF71Dt8dIIsIw1QtJ2MgLC0nKjcqLC0wYywlYzYwJmnFC8W0TkJCRkZDcyFySHNKRUAWbHPCgEVLaEVCRkZEQUFzwvVZwvBvYyAmMTcqJSogIjcmYzMsLyogOkdFUEEWEHJQc1JFQBZHSVBJAjMzbQPltAQOPEsdc1xFQBZeYEdbc1UhLyZjMDciLSciMSdjNyYxLjBjIiolKiAiNyosLWMCNjcrLDEqNzpySx1zwUJSRUAWXmNHwUJLc8FCR3M8AuvbupKJJd9nKFKT4PinWGmAXAY9XA8oE9UCyoc3IUhTwALEcMnCXNKYXQQTqEauHTrHbqh14RQPFq8RJi8qIi0gJmMsLWM3KyowYyAmMYpaMbYeTZY8HNixZkD5FswOHk6ygyBwNLR5RG8VqJlMYk2Z+TBaDPb2ee63TE1D0UjyYlVtN5Z/TpghVXPBR/hzwUDg40BBQkFBQkFzTkVKMy8mYwAmMTcqJSogIjcqLC1jAjb9tzDYrZEnTIg6DHeb4X26O7woi8FCQ0VKacULxbQgJ0ZCc8Kxc2lFNDRtIjMzLyZtICwubCIzMy8mICIzLyZjESwsN2MAAnNdVE5zdXN3cXNSRUAWR0lQSQIzMy8mYwotIG1ycHUZcyFySHNKRUAWR0VQQRYQclAndmBWCFYaXvDXtLXf3YwT+YIbE0ZDQMFCTENzwUJJQcFCQkOn0upKmnU8gsQWmuTa+nEBuJuWMt094hExIiA3KiAmYzA3IjcmLiYtNzBtcyTMS/djtIjvb2MsM/V8QnPP9ACMRK8+esDIEGOQe4fy/NkMSSi8aL8vJmMKLSBtcmVzZ0VAFkdIUF4CM2MsJWM3KyZjNysmLWMiMzMvKiAi8nMbrxlHcc8r8MxenSYwvCQdJv8KmzXccFcm4jTXim5BQEJDQuDBQktoRUJGRkRBQlVdKzc3MzB5bGw0zDDCI4VYGEps0fG7BwuzI3vdVrZVc1dFQBZHQFBOAjMzLyZjESwsNzlzwUI1c01FQBZeTEJCvEdHQEFCOmMiMDA2LiYwYyIgICYzNyItICZnoaiS9DOcTAaiZImyLjuupPZUVGMAAnPBQmFzTkVKacULxbROQkJCGuRGSj9UAxVSXTeQ9MhgeATglixcxsDGWNp+BHSx6tgDzW+X8tNRm3XaD2479K7P2J+wNNixNZE0cwyCE+nJlpmnv5NKRHTzNjZi");
        private static int[] order = new int[] { 24,54,56,16,47,5,32,29,45,18,10,22,29,30,39,59,43,54,37,44,56,31,42,42,33,54,54,59,38,44,57,42,33,47,50,50,36,47,47,45,56,45,58,46,58,51,46,51,54,59,50,55,58,55,56,55,57,58,59,59,60 };
        private static int key = 67;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
