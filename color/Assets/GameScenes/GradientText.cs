﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GradientText : MonoBehaviour
{
    public RawImage imag;

    int texWidth = 2048;
    int texHeight = 2048;

    //MASK SETTINGS
    public float maskThreshold = 2.0f;

    //REFERENCES
    Texture2D mask;

    void Start()
    {
        GenerateTexture();
    }

    void GenerateTexture()
    {
        mask = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, true);
        Vector2 maskCenter = new Vector2(texWidth * 0.5f, texHeight * 0.5f);

        for (int y = 0; y < texHeight; ++y)
        {
            for (int x = 0; x < texWidth; ++x)
            {

                var distFromCenter = Vector2.Distance(maskCenter, new Vector2(x, y));
                float maskPixel = (0.5f - (distFromCenter / texWidth)) * maskThreshold;
                mask.SetPixel(x, y, new Color(maskPixel, maskPixel, maskPixel, 1.0f));
            }
        }
        mask.Apply();
        imag.texture = mask;
        File.WriteAllBytes(Application.persistentDataPath + "Small.png", mask.EncodeToPNG());
    }
}
