﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToastController : MonoBehaviour
{
    public Color[] BG_Colors;
    public Sprite[] icons;
    private string[] descriptions = { "New Image Downloaded", "Today's Downloading Complete" };
    public Text description_object;
    public Image icon_object;
    // Start is called before the first frame update

    void Start()
    {

        Close_Toast();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Close_Toast()
    {
        iTween.ScaleTo(gameObject, new Vector3(1, 0, 1), 0.5f);
        CancelInvoke();
    }
    public void Show_Toast()
    {
        iTween.ScaleTo(gameObject, Vector3.one, 0.5f);
        CancelInvoke();

    }
    public void downloading_images()
    {
        description_object.text = descriptions[0];
        icon_object.sprite = icons[0];
        gameObject.GetComponent<Image>().color = BG_Colors[0];
        Show_Toast();
        Invoke("Close_Toast", 1.5f);
    }
    public void downloading_complete()
    {
        description_object.text = descriptions[1];
        icon_object.sprite = icons[1];
        gameObject.GetComponent<Image>().color = BG_Colors[1];
        Show_Toast();
        Invoke("Close_Toast", 3f);
    }
}
