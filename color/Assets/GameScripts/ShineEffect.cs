﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShineEffect : MonoBehaviour
{

	public float xSpeed = 0.0f;
	public float ySpeed = 0.0f;
	public float zSpeed = 0.0f;
	private bool scaleUp = false;
	public static int hides = 0;
	public static float starMaxScale = 0.9f;
	public float speed;

	void Start ()
	{
		Invoke ("reverseDirection", Random.Range (3, 7));
//		Invoke ("hideShow", Random.Range (1, 3));
	}

	void Update ()
	{
		speed = Random.Range (25,35);
		transform.Rotate (
			xSpeed * Time.deltaTime,
			ySpeed * Time.deltaTime,
			zSpeed * Time.deltaTime
		);
		if (transform.localScale.x < 0.1f) {
			scaleUp = true;
		}
		if (transform.localScale.x > starMaxScale) {
			scaleUp = false;
		}
		if (scaleUp) {
			transform.localScale += new Vector3 (0.05f, 0.05f, 0.05f) * (starMaxScale * speed) * Time.deltaTime;
		} else {
			transform.localScale -= new Vector3 (0.05f, 0.05f, 0.05f) * (starMaxScale * speed) * Time.deltaTime;

		}
	}

	void reverseDirection ()
	{
		zSpeed *= -1;
		Invoke ("reverseDirection", Random.Range (3, 7));

	}

	void hideShow ()
	{
		if (gameObject.activeInHierarchy) {
			if (hides < 6) {
				hides++;
				gameObject.SetActive (!gameObject.activeInHierarchy);
			}
		} else {
			hides--;
			gameObject.SetActive (!gameObject.activeInHierarchy);

		}

		Invoke ("hideShow", Random.Range (1, 3));

	}
}
