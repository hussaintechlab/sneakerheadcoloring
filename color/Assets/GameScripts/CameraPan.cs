﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CameraPan : MonoBehaviour
{
    private Vector3 lastPosition;

    public readonly float PanSpeed = 1200f;
    private readonly float ZoomSpeedTouch = 0.8f;
    private readonly float ZoomSpeedMouse = 8f;

    public readonly float[] BoundsX = new float[] { -490f, 490f };
    public readonly float[] BoundsY = new float[] { -500f, 500f };
    public readonly float[] ZoomBounds = new float[] { 25f, 1300f };
    //
    private Camera cam;

    private Vector2 lastPanPosition;
    private int panFingerId;
    // Touch mode only

    private bool wasZoomingLastFrame;
    // Touch mode only
    private Vector2[] lastZoomPositions;
    // Touch mode only
    // Touch mode only
    // Use this for initialization
    void Awake()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        ShineEffect.starMaxScale = Mathf.Clamp((cam.orthographicSize / ZoomBounds[1] * 1.5f), 0.25f, 1f);
        if (GameConfig.zoomPanEnable)
        {
            if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer)
            {
                HandleTouch();
            }
            else
            {
                HandleMouse();
            }
        }
    }

    void HandleTouch()
    {
        switch (Input.touchCount)
        {

            case 1: // Panning

                // If the touch began, capture its position and its finger ID.
                // Otherwise, if the finger ID of the touch doesn't match, skip it.
                if (!wasZoomingLastFrame && cam.orthographicSize < 1200)
                {
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began)
                    {
                        lastPanPosition = touch.position;
                        panFingerId = touch.fingerId;
                    }
                    else if (touch.fingerId == panFingerId && touch.phase == TouchPhase.Moved)
                    {
                        if (EventSystem.current.currentSelectedGameObject == null)
                        {
                            PanCamera(touch.position);
                        }
                    }
                }
                wasZoomingLastFrame = false;
                break;

            case 2: // Zooming
                Touch touch0 = Input.GetTouch(0);
                Touch touch1 = Input.GetTouch(1);
                Vector2[] newPositions = new Vector2[] { touch0.position, touch1.position };
                if (!wasZoomingLastFrame)
                {
                    lastZoomPositions = newPositions;
                    wasZoomingLastFrame = true;
                }
                else
                {
                    // Zoom based on the distance between the new positions compared to the
                    // distance between the previous positions.
                    float newDistance = Vector2.Distance(newPositions[0], newPositions[1]);
                    float oldDistance = Vector2.Distance(lastZoomPositions[0], lastZoomPositions[1]);
                    float offset = newDistance - oldDistance;

                    //Hack for zoom jerk on up
                    if (touch0.phase == TouchPhase.Ended || touch1.phase == TouchPhase.Ended)
                    {
                        offset = 0;
                    }

                    ZoomCamera(offset, ZoomSpeedTouch);
                    lastZoomPositions = newPositions;
                }
                break;

            default:
                wasZoomingLastFrame = false;
                break;
        }
    }

    void HandleMouse()
    {
        // On mouse down, capture it's position.
        // Otherwise, if the mouse is still down, pan the camera.
        if (Input.GetMouseButtonDown(0))
        {
            lastPanPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0) && EventSystem.current.currentSelectedGameObject == null)
        {
            PanCamera(Input.mousePosition);
        }

        // Check for scrolling to zoom the camera
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        ZoomCamera(scroll, ZoomSpeedMouse);
    }

    void PanCamera(Vector2 newPanPosition)
    {
        // Determine how much to move the camera
        if (Vector2.Distance(lastPanPosition, newPanPosition) > 10)
        {
            Vector2 offset = cam.ScreenToViewportPoint(lastPanPosition - newPanPosition);
            float newPanSpeed = Mathf.Clamp(cam.orthographicSize * 1.7f, 70f, ZoomBounds[1]);

            Vector3 move = new Vector3(offset.x * newPanSpeed, offset.y * newPanSpeed, 0);
            // Perform the movement
            transform.Translate(move, Space.World);

            // Ensure the camera remains within bounds.
            Vector3 pos = transform.position;

            pos.x = Mathf.Clamp(transform.position.x, (BoundsX[0]), (BoundsX[1]));
            pos.y = Mathf.Clamp(transform.position.y, (BoundsY[0]), (BoundsY[1]));
            transform.position = pos;

            // Cache the position
            lastPanPosition = newPanPosition;
        }
    }

    void ZoomCamera(float offset, float speed)
    {
        if (offset == 0)
        {
            return;
        }
        cam.orthographicSize = Mathf.Clamp(cam.orthographicSize - (offset * speed), ZoomBounds[0], ZoomBounds[1]);
    }
}
