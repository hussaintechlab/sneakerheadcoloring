// Optimized Mobile Painter - Unitycoder.com

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.IO;
using System.Runtime.InteropServices;
using System;
using UnityEngine.UI;
using MoreMountains.NiceVibrations;

public class Painting : MonoBehaviour
{
    public enum DrawMode
    {
        Eraser,
        Solid,
        Gredient,
        Live,
        Alpha,
        Marker,
        Crayon,
        Pencil,
        Spray
    }
    public DrawMode drawMode = DrawMode.Solid;
    public bool enableMouse = true;
    public bool pixelsUpdated = false;
    public LayerMask paintLayerMask;
    public int limit;


    public Color32 paintColor = new Color32(255, 0, 0, 255);
    public byte paintThreshold = 128;
    public float movementThreshhold;

    private byte[] lockMaskPixels;
    string targetTexture = "_MainTex";

    public Color32 clearColor = new Color32(255, 255, 255, 255);
    public Texture2D gredientTex;
    Texture2D maskTex;


    private byte[] pixels;
    private byte[] maskPixels;
    private byte[] gredientPixels;
    private byte[] rawPixels;
    private byte[] undoPixels;
    private byte[] redoPixels;
    private byte[] maskEffectPixels;
    private byte[] pencilPixels;
    private byte[] crayonPixels;
    private byte[] patternPixels;
    private byte[] overlayPixels;
    private List<byte[]> pixelList = new List<byte[]>();
    private List<bool> pixelListTrack = new List<bool>();

    private Texture2D tex;
    private Texture2D overlayTex;
    private int texWidth;
    private int texHeight;
    private Camera cam;
    private RaycastHit hit;
    private bool wentOutside = false;
    private bool canRedoLastPixels = false;

    private Vector2 pixelUV;
    private Vector2 pixelUVOld;

    [HideInInspector]
    public bool textureNeedsUpdate = false;
    [HideInInspector]
    public bool overlayNeedsUpdate = false;

    bool clickable = false;
    Vector2 clickPos;
    private List<Vector3> pointsUnderFinger;
    private List<int> randomPositions;
    float circleOffset = 0.1f;
    bool shouldBreak = false;
    List<int> pixels_of_current_area = new List<int>();
    Color32 localColor;
    Color32 foundColor;
    int basePixel = 0;
    private int brushSizeX1 = 48; // << 1
    private int brushSizeXbrushSize = 576; // x*x
    private int brushSizeX4 = 96; // << 2
    public int brushSize = 24; // default brush size
    private int brushSizeDiv4 = 6; // >> 2 == /4
    private float alphaLerpVal = 0.1f;
    private float brushAlphaStrengthVal = 0.01f; // cached calculation
    bool progressExist = false;
    bool overlayExist = false;
    private int currentIndex = 0;
    private bool saveLastState = true;
    private RegionsManager uiManager;
    public static Painting instance;
    Color32 colorToUse;
    public Texture2D crayonTex, pencilTex;
    bool isBrushTool = false;
    public GameObject overlayMesh;
    public GameObject backlayerMesh;
    public GameObject patternMesh;
    public bool isGredient = false;
    public Slider brushSlider;

    void Awake()
    {
        instance = this;
        cam = Camera.main;
        uiManager = GetComponent<RegionsManager>();
        texWidth = texHeight = 1024;
        brushAlphaStrengthVal = 255f / 0.1f;
        alphaLerpVal = paintColor.a / brushAlphaStrengthVal; // precalc
        InitializeEverything();
    }

    public void InitializeEverything()
    {
        pointsUnderFinger = new List<Vector3>();
        randomPositions = new List<int>();
        Texture2D loadedMask = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, false);
        //if (GameConfig.regionNumber < DownloadManager.offLineImages)
        //{
        loadedMask = Resources.Load(GameConfig.folderName + "/Large/" + GameConfig.regionNumber) as Texture2D;
        //}
        //else
        //{
        //    loadedMask.LoadImage(File.ReadAllBytes(GameConfig.regionIconPath() + "/Large/" + GameConfig.regionNumber + ".png"));
        //}

        string progressPath = GameConfig.dataPath() + "/Back/" + GameConfig.folderName + "_" + GameConfig.regionNumber + ".png";
        string overlay_progressPath = GameConfig.dataPath() + "/Overlay/" + GameConfig.folderName + "_" + GameConfig.regionNumber + ".png";
        Texture2D tex_target = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, false);
        Texture2D tex_overlay = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, false);
        if (File.Exists(progressPath))
        {
            progressExist = true;
            tex_target.LoadImage(File.ReadAllBytes(progressPath));
            GetComponent<Renderer>().material.SetTexture(targetTexture, tex_target);
        }
        if (File.Exists(overlay_progressPath))
        {
            overlayExist = true;
            tex_overlay.LoadImage(File.ReadAllBytes(overlay_progressPath));
            overlayMesh.GetComponent<Renderer>().material.SetTexture(targetTexture, tex_overlay);
        }

        pixels = new byte[texWidth * texHeight * 4];
        overlayPixels = new byte[texWidth * texHeight * 4];
        tex = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, false);
        maskTex = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, false);
        overlayTex = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, false);
        maskTex.SetPixels32(loadedMask.GetPixels32());
        maskTex.Apply(false);

        if (GetComponent<Renderer>().material.GetTexture(targetTexture) != null)
        {
            tex.SetPixels32(((Texture2D)GetComponent<Renderer>().material.GetTexture(targetTexture)).GetPixels32());
            tex.Apply(false);
            Color32ArrayToByteArray(ref pixels, tex);
        }
        if (overlayMesh.GetComponent<Renderer>().material.GetTexture(targetTexture) != null)
        {
            overlayTex.SetPixels32(((Texture2D)overlayMesh.GetComponent<Renderer>().material.GetTexture(targetTexture)).GetPixels32());
            overlayTex.Apply(false);
            Color32ArrayToByteArray(ref overlayPixels, overlayTex);
        }
        GetComponent<Renderer>().material.SetTexture("_MaskTex", maskTex);
        GetComponent<Renderer>().material.SetTexture(targetTexture, tex);
        overlayMesh.GetComponent<Renderer>().material.SetTexture(targetTexture, overlayTex);
        CreateFullScreenQuad(GetComponent<MeshFilter>().mesh);
        CreateFullScreenQuad(overlayMesh.GetComponent<MeshFilter>().mesh);
        CreateFullScreenQuad(backlayerMesh.GetComponent<MeshFilter>().mesh);
        CreateFullScreenQuad(patternMesh.GetComponent<MeshFilter>().mesh);
        // add mesh collider
        gameObject.AddComponent<MeshCollider>();
        // adjust cam size
        cam.orthographicSize = (Screen.height / 2f) + (Screen.height * 0.075f);
        Vector2 xyHeight = new Vector2(0, (RegionsManager.instance.top.position.y + RegionsManager.instance.bottom.position.y) / 2f);
        transform.position = new Vector3(transform.position.x, cam.ScreenToWorldPoint(xyHeight).y, transform.position.z);
        ClearImage();
        //undoPixels = new byte[texWidth * texHeight * 4];
        //System.Array.Copy(pixels, undoPixels, pixels.Length);
        tex.filterMode = FilterMode.Point;
        tex.wrapMode = TextureWrapMode.Clamp;
        maskTex.filterMode = FilterMode.Point;
        maskTex.wrapMode = TextureWrapMode.Clamp;
        overlayTex.filterMode = FilterMode.Point;
        overlayTex.wrapMode = TextureWrapMode.Clamp;
        uiManager.drawing.texture = tex;
        uiManager.outline.texture = maskTex;
        uiManager.overlay.texture = overlayTex;
    }

    // *** MAINLOOP ***
    void Update()
    {
        if (enableMouse)
            MousePaint();

        if (currentIndex == pixelList.Count && pixelList.Count > 0)
        {
            uiManager.updateUndoUI(1);
            uiManager.updateRedoUI(0);
        }
        else if (currentIndex > 0 && currentIndex < pixelList.Count)
        {
            if (currentIndex == pixelList.Count - 1)
            {
                uiManager.updateUndoUI(1);
                uiManager.updateRedoUI(0);
            }
            else
            {
                uiManager.updateRedoUI(1);
                uiManager.updateUndoUI(1);
            }
        }
        else if (currentIndex == 0)
        {
            uiManager.updateUndoUI(0);
            if (pixelList.Count > 0)
            {
                uiManager.updateRedoUI(1);
            }
        }
        UpdateTexture();
    }

    void MousePaint()
    {
        if (EventSystem.current.currentSelectedGameObject != null)
            return;

        if (Application.isEditor)
        {
            clickable = true;
        }
        else
        {
            if (Input.touchCount == 1)
            {
                clickable = true;
            }
            else
            {
                clickable = false;
            }
        }

        if (!clickable)
        {
            return;
        }

        if (isBrushTool)
        {
            if (Input.GetMouseButtonDown(0))
            {
                colorToUse = paintColor;
                if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, paintLayerMask))
                    return;
                savePixelsForRedoUndo();
                //GetPixelCountOfArea((int)(hit.textureCoord.x * texWidth), (int)(hit.textureCoord.y * texHeight));
            }

            if (Input.GetMouseButton(0))
            {
                if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, paintLayerMask))
                {
                    wentOutside = true;
                    return;
                }

                if (drawMode == DrawMode.Spray)
                {
                    Vector2 nowHit = hit.textureCoord;
                    nowHit.x *= texWidth;
                    nowHit.y *= texHeight;
                    if (Vector2.Distance(nowHit, pixelUVOld) > 12)
                    {
                        float shiftOffset = 0f;
                        pointsUnderFinger.Clear();
                        for (int i = 0; i < 20; i++)
                        {
                            pointsUnderFinger.Add(Input.mousePosition);
                            pointsUnderFinger.Add(Input.mousePosition + new Vector3(shiftOffset + circleOffset, 0, 0));
                            pointsUnderFinger.Add(Input.mousePosition + new Vector3(-shiftOffset - circleOffset, 0, 0));
                            pointsUnderFinger.Add(Input.mousePosition + new Vector3(0, shiftOffset + circleOffset, 0));
                            pointsUnderFinger.Add(Input.mousePosition + new Vector3(0, -shiftOffset - circleOffset, 0));
                            pointsUnderFinger.Add(Input.mousePosition + new Vector3((shiftOffset + circleOffset) / 2, (shiftOffset + circleOffset) / 2, 0));
                            pointsUnderFinger.Add(Input.mousePosition + new Vector3((-shiftOffset - circleOffset) / 2, (-shiftOffset - circleOffset) / 2, 0));
                            pointsUnderFinger.Add(Input.mousePosition + new Vector3((shiftOffset + circleOffset) / 2, (-shiftOffset - circleOffset) / 2, 0));
                            pointsUnderFinger.Add(Input.mousePosition + new Vector3((-shiftOffset - circleOffset) / 2, (shiftOffset + circleOffset) / 2, 0));
                            shiftOffset += 2.25f + (((cam.orthographicSize - 1024) * -1) / 924);
                        }
                        for (int i = 0; i < pointsUnderFinger.Count;)
                        {
                            if (i >= pointsUnderFinger.Count)
                            {
                                break;
                            }
                            if (!Physics.Raycast(cam.ScreenPointToRay(pointsUnderFinger[i]), out hit, Mathf.Infinity, paintLayerMask))
                            {
                                pointsUnderFinger.RemoveAt(i);
                            }
                            else
                            {
                                i++;
                            }
                        }
                        if (pointsUnderFinger.Count == 0)
                        {
                            wentOutside = true;
                            return;
                        }

                        int randPositionCount = UnityEngine.Random.Range(8, 15);
                        randomPositions.Clear();
                        while (randomPositions.Count != randPositionCount)
                        {
                            randomPositions.Add(UnityEngine.Random.Range(0, pointsUnderFinger.Count));
                        }
                        for (var i = 0; i < randPositionCount; i++)
                        {
                            if (!Physics.Raycast(cam.ScreenPointToRay(pointsUnderFinger[randomPositions[i]]), out hit, Mathf.Infinity, paintLayerMask))
                            {
                                wentOutside = true;
                                continue;
                            }

                            Vector2 point = hit.textureCoord;
                            point.x *= texWidth;
                            point.y *= texHeight;

                            brushSize = UnityEngine.Random.Range(3, (int)Mathf.Clamp(brushSlider.value, 4, brushSlider.maxValue));
                            brushSizeX1 = brushSize << 1;
                            brushSizeXbrushSize = brushSize * brushSize;
                            brushSizeX4 = brushSizeXbrushSize << 2;
                            brushSizeDiv4 = brushSize >> 2;

                            colorToUse = new Color32(colorToUse.r, colorToUse.g, colorToUse.b, (byte)UnityEngine.Random.Range(200, 255));
                            DrawCircle((int)point.x, (int)point.y);
                        }
                        pixelUV = nowHit;
                        pixelUVOld = pixelUV;
                    }
                }
                else
                {
                    pixelUVOld = pixelUV; // take previous value, so can compare them
                    pixelUV = hit.textureCoord;
                    pixelUV.x *= texWidth;
                    pixelUV.y *= texHeight;

                    if (wentOutside)
                    {
                        pixelUVOld = pixelUV;
                        wentOutside = false;
                    }

                    DrawCircle((int)pixelUV.x, (int)pixelUV.y);
                }
                overlayNeedsUpdate = true;
            }
        }

        if (Input.GetMouseButtonUp(0) && !isBrushTool)
        {
            if (Vector2.Distance(clickPos, Input.mousePosition) <= movementThreshhold)
            {
                // Only if we hit something, then we continue
                float shiftOffset = 0f;
                pointsUnderFinger.Clear();
                for (int i = 0; i < 20; i++)
                {
                    pointsUnderFinger.Add(Input.mousePosition);
                    pointsUnderFinger.Add(Input.mousePosition + new Vector3(shiftOffset + circleOffset, 0, 0));
                    pointsUnderFinger.Add(Input.mousePosition + new Vector3(-shiftOffset - circleOffset, 0, 0));
                    pointsUnderFinger.Add(Input.mousePosition + new Vector3(0, shiftOffset + circleOffset, 0));
                    pointsUnderFinger.Add(Input.mousePosition + new Vector3(0, -shiftOffset - circleOffset, 0));
                    pointsUnderFinger.Add(Input.mousePosition + new Vector3((shiftOffset + circleOffset) / 2, (shiftOffset + circleOffset) / 2, 0));
                    pointsUnderFinger.Add(Input.mousePosition + new Vector3((-shiftOffset - circleOffset) / 2, (-shiftOffset - circleOffset) / 2, 0));
                    pointsUnderFinger.Add(Input.mousePosition + new Vector3((shiftOffset + circleOffset) / 2, (-shiftOffset - circleOffset) / 2, 0));
                    pointsUnderFinger.Add(Input.mousePosition + new Vector3((-shiftOffset - circleOffset) / 2, (shiftOffset + circleOffset) / 2, 0));
                    shiftOffset += 2f + (((cam.orthographicSize - 1300) * -1) / 1200);
                }
                for (int i = 0; i < pointsUnderFinger.Count;)
                {
                    if (i >= pointsUnderFinger.Count)
                    {
                        break;
                    }
                    if (!Physics.Raycast(cam.ScreenPointToRay(pointsUnderFinger[i]), out hit, Mathf.Infinity, paintLayerMask))
                    {
                        pointsUnderFinger.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
                if (pointsUnderFinger.Count == 0)
                {
                    wentOutside = true;
                    return;
                }

                for (int i = 0; i < pointsUnderFinger.Count; i++)
                {
                    if (!Physics.Raycast(cam.ScreenPointToRay(pointsUnderFinger[i]), out hit, Mathf.Infinity, paintLayerMask))
                    {
                        wentOutside = true;
                        return;
                    }
                    pixelUVOld = pixelUV; // take previous value, so can compare them
                    pixelUV = hit.textureCoord;
                    pixelUV.x *= texWidth;
                    pixelUV.y *= texHeight;

                    if (wentOutside)
                    {
                        pixelUVOld = pixelUV;
                        wentOutside = false;
                    }

                    checkColor(pointsUnderFinger[i]);
                    if (foundColor.r == clearColor.r && foundColor.g == clearColor.g && foundColor.b == clearColor.b)
                    {
                        colorToUse = paintColor;
                    }
                    else
                    {
                        if (foundColor.r == paintColor.r && foundColor.g == paintColor.g && foundColor.b == paintColor.b)
                        {
                            colorToUse = new Color(1f, 1f, 1f, 0f);
                        }
                        else
                        {
                            colorToUse = paintColor;
                        }
                    }

                    FloodFillMaskOnlyWithThreshold((int)pixelUV.x, (int)pixelUV.y);
                    if (shouldBreak)
                    {
                        shouldBreak = false;
                        break;
                    }
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            // take this position as start position
            if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, paintLayerMask))
                return;
            pixelUVOld = pixelUV;
            clickPos = Input.mousePosition;
            if (!GameConfig.frontColored)
            {
                GameConfig.frontColored = isBrushTool;
            }
            if (!GameConfig.backColored)
            {
                GameConfig.backColored = !isBrushTool;
            }
        }

        if (Vector2.Distance(pixelUV, pixelUVOld) > brushSize && isBrushTool && drawMode != DrawMode.Spray)
        {
            DrawLine((int)pixelUVOld.x, (int)pixelUVOld.y, (int)pixelUV.x, (int)pixelUV.y);
            pixelUVOld = pixelUV;
            overlayNeedsUpdate = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (canRedoLastPixels == false)
            {
                redoPixels = new byte[texWidth * texHeight * 4];
                if (isBrushTool)
                {
                    System.Array.Copy(overlayPixels, redoPixels, pixels.Length);
                }
                else
                {
                    System.Array.Copy(pixels, redoPixels, pixels.Length);
                }
            }
        }
    }

    void UpdateTexture()
    {
        if (textureNeedsUpdate)
        {
            textureNeedsUpdate = false;
            tex.LoadRawTextureData(pixels);
            tex.Apply(false);
        }
        if (overlayNeedsUpdate)
        {
            overlayNeedsUpdate = false;
            overlayTex.LoadRawTextureData(overlayPixels);
            overlayTex.Apply(false);
        }
    }

    bool CompareThreshold(byte a, byte b)
    {
        //return Mathf.Abs(a-b)<=threshold;
        if (a < b)
        {
            a ^= b;
            b ^= a;
            a ^= b;
        } // http://lab.polygonal.de/?p=81
        return (a - b) <= paintThreshold;
    }

    Color32 white = Color.white;
    string status = "";
    public void DrawPoint(int pixel)
    {
        if (drawMode == DrawMode.Gredient)
        {
            if (colorToUse.Equals(clearColor))
            {
                copyColor(pixel);
            }
            else
            {
                int row = ((pixel / 4) / texWidth);
                int col = ((pixel / 4) % texWidth);
                row += 512;
                col += 512;
                int newGDPixel = ((2048 * col) + row) * 4;
                int row1 = ((basePixel / 4) / texWidth);
                int col1 = ((basePixel / 4) % texWidth);
                row1 += 512;
                col1 += 512;
                int newGDPixel1 = ((2048 * col1) + row1) * 4;

                //16777212
                int newPixel = 8392704 + (newGDPixel - newGDPixel1);
                //int rawPixel = Mathf.Abs(2099200 + (pixel - basePixel));
                //int rawPixel = 2099200 - pixel;
                //int newPixel = Mathf.Clamp(rawPixel, 0, 4194300);
                //if (rawPixel > 4194300 || rawPixel < 0)
                //{
                //    status += rawPixel + " " + newPixel + "\n";
                //}
                pixels[pixel] = (byte)((colorToUse.r + gredientPixels[newPixel]) / 2);
                pixels[pixel + 1] = (byte)((colorToUse.g + gredientPixels[newPixel + 1]) / 2);
                pixels[pixel + 2] = (byte)((colorToUse.b + gredientPixels[newPixel + 2]) / 2);
                pixels[pixel + 3] = (byte)((colorToUse.a + gredientPixels[newPixel + 3]) / 2);
            }
        }
        else
        {
            int newPixel = (int)Mathf.Repeat(131584 + (pixel - basePixel), 262140);
            if (drawMode == DrawMode.Alpha)
            {
                overlayPixels[pixel] = ByteLerp(overlayPixels[pixel], colorToUse.r, 0.1f);
                overlayPixels[pixel + 1] = ByteLerp(overlayPixels[pixel + 1], colorToUse.g, 0.1f);
                overlayPixels[pixel + 2] = ByteLerp(overlayPixels[pixel + 2], colorToUse.b, 0.1f);
                overlayPixels[pixel + 3] = ByteLerp(overlayPixels[pixel + 3], colorToUse.a, 0.1f);
            }
            else if (drawMode == DrawMode.Crayon)
            {
                overlayPixels[pixel] = ByteLerp(overlayPixels[pixel], colorToUse.r, 0.2f);
                overlayPixels[pixel + 1] = ByteLerp(overlayPixels[pixel + 1], colorToUse.g, 0.2f);
                overlayPixels[pixel + 2] = ByteLerp(overlayPixels[pixel + 2], colorToUse.b, 0.2f);
                overlayPixels[pixel + 3] = ByteLerp(overlayPixels[pixel + 3], crayonPixels[newPixel + 3], 0.2f);
            }
            else if (drawMode == DrawMode.Pencil)
            {
                overlayPixels[pixel] = colorToUse.r;
                overlayPixels[pixel + 1] = colorToUse.g;
                overlayPixels[pixel + 2] = colorToUse.b;
                overlayPixels[pixel + 3] = pencilPixels[newPixel];
                //overlayPixels[pixel + 3] = ByteLerp(overlayPixels[pixel + 3], (byte)(pencilPixels[newPixel] - (byte)50f), 1f);
            }
            else if (drawMode == DrawMode.Marker)
            {
                overlayPixels[pixel] = colorToUse.r;
                overlayPixels[pixel + 1] = colorToUse.g;
                overlayPixels[pixel + 2] = colorToUse.b;
                overlayPixels[pixel + 3] = 128;
            }
            else
            {
                copyColor(pixel);
            }
        }
        copyColor(pixel, true);
    }

    void copyColor(int pixel, bool isRaw = false)
    {
        if (isRaw)
        {
            rawPixels[pixel] = colorToUse.r;
            rawPixels[pixel + 1] = colorToUse.g;
            rawPixels[pixel + 2] = colorToUse.b;
            rawPixels[pixel + 3] = (byte)255;
        }
        else
        {
            if (isBrushTool)
            {
                overlayPixels[pixel] = colorToUse.r;
                overlayPixels[pixel + 1] = colorToUse.g;
                overlayPixels[pixel + 2] = colorToUse.b;
                overlayPixels[pixel + 3] = colorToUse.a;
            }
            else
            {
                pixels[pixel] = colorToUse.r;
                pixels[pixel + 1] = colorToUse.g;
                pixels[pixel + 2] = colorToUse.b;
                pixels[pixel + 3] = colorToUse.a;
            }
        }
    }

    // init/clear image, this can be called outside this script also
    public void ClearImage()
    {
        maskPixels = new byte[texWidth * texHeight * 4];
        maskEffectPixels = new byte[texWidth * texHeight * 4];
        rawPixels = new byte[texWidth * texHeight * 4];

        int pixel = 0;
        for (int y = 0; y < texHeight; y++)
        {
            for (int x = 0; x < texWidth; x++)
            {
                if (!progressExist)
                {
                    pixels[pixel] = clearColor.r;
                    pixels[pixel + 1] = clearColor.g;
                    pixels[pixel + 2] = clearColor.b;
                    pixels[pixel + 3] = clearColor.a;
                }
                if (!overlayExist)
                {
                    overlayPixels[pixel] = clearColor.r;
                    overlayPixels[pixel + 1] = clearColor.g;
                    overlayPixels[pixel + 2] = clearColor.b;
                    overlayPixels[pixel + 3] = clearColor.a;
                }
                Color c = maskTex.GetPixel(x, y);
                if (c.a == 0)
                {
                    c = Color.clear;
                }
                maskPixels[pixel] = (byte)(c.r * 255);
                maskPixels[pixel + 1] = (byte)(c.g * 255);
                maskPixels[pixel + 2] = (byte)(c.b * 255);
                maskPixels[pixel + 3] = (byte)(c.a * 255);
                pixel += 4;
            }
        }

        Color32ArrayToByteArray(ref gredientPixels, gredientTex);
        Color32ArrayToByteArray(ref pencilPixels, pencilTex);
        Color32ArrayToByteArray(ref crayonPixels, crayonTex);

        System.Array.Copy(pixels, rawPixels, pixels.Length);
        System.Array.Copy(maskPixels, maskEffectPixels, maskPixels.Length);

        if (!progressExist)
        {
            tex.LoadRawTextureData(pixels);
            tex.Apply(false);
        }
        if (!overlayExist)
        {
            overlayTex.LoadRawTextureData(overlayPixels);
            overlayTex.Apply(false);
        }
    }
    // clear image

    void CreateFullScreenQuad(Mesh go_Mesh)
    {
        // create mesh plane, fits in camera view (with screensize adjust taken into consideration)
        go_Mesh.Clear();
        go_Mesh.vertices = new[] {
                  new Vector3 (-Screen.width / 2, -Screen.width / 2, cam.nearClipPlane + 0.1f), // bottom left
        	new Vector3 (-Screen.width / 2, Screen.width / 2, cam.nearClipPlane + 0.1f), // top left
        	new Vector3 (Screen.width / 2, Screen.width / 2, cam.nearClipPlane + 0.1f), // top right
        	new Vector3 (Screen.width / 2, -Screen.width / 2, cam.nearClipPlane + 0.1f) // bottom right
        };
        go_Mesh.uv = new[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0) };
        go_Mesh.triangles = new[] { 0, 1, 2, 0, 2, 3 };
        // TODO: add option for this
        go_Mesh.RecalculateNormals();
        // TODO: add option to calculate tangents
        go_Mesh.tangents = new[] {
            new Vector4 (1.0f, 0.0f, 0.0f, -1.0f),
            new Vector4 (1.0f, 0.0f, 0.0f, -1.0f),
            new Vector4 (1.0f, 0.0f, 0.0f, -1.0f),
            new Vector4 (1.0f, 0.0f, 0.0f, -1.0f)
        };
    }

    public void checkColor(Vector3 point)
    {
        Physics.Raycast(cam.ScreenPointToRay(point), out hit, Mathf.Infinity);
        Vector2 hitCoord = hit.textureCoord;
        hitCoord.x *= texWidth;
        hitCoord.y *= texHeight;
        int pixel = (texWidth * (int)hitCoord.y + (int)hitCoord.x) * 4;
        foundColor = new Color32(rawPixels[pixel], rawPixels[pixel + 1], rawPixels[pixel + 2], rawPixels[pixel + 3]);
    }

    bool speedCheck = false;
    public void FloodFillMaskOnlyWithThreshold(int x, int y)
    {
        pixels_of_current_area.Clear();
        int total_pixels = GetPixelCountOfArea(x, y, true);
        if (total_pixels == 0)
        {
            return;
        }
        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
        savePixelsForRedoUndo();
        shouldBreak = true;
        speedCheck = true;
        StartCoroutine(ApplyFloodFillWithDelay(0, (total_pixels * 0.00001f)));
    }

    IEnumerator ApplyFloodFillWithDelay(int index, float speed)
    {
        enableMouse = false;
        if (speedCheck)
        {
            speed = 6;
            speedCheck = false;
        }

        bool isLoopBreak = false;
        int i = 0;
        yield return new WaitForEndOfFrame();
        while (i < (int)speed)
        {
            DrawPoint(pixels_of_current_area[index]);
            index++;
            if (index >= pixels_of_current_area.Count)
            {
                isLoopBreak = true;
                break;
            }
            i++;
        }

        if (!isLoopBreak)
        {
            StartCoroutine(ApplyFloodFillWithDelay(index, speed + (speed * 0.3f)));
        }
        else
        {
            enableMouse = true;
        }
        textureNeedsUpdate = true;
    }

    int GetPixelCountOfArea(int x, int y, bool calOnly = false)
    {
        byte hitColorR = maskPixels[((texWidth * (y) + x) * 4) + 0];
        byte hitColorG = maskPixels[((texWidth * (y) + x) * 4) + 1];
        byte hitColorB = maskPixels[((texWidth * (y) + x) * 4) + 2];
        byte hitColorA = maskPixels[((texWidth * (y) + x) * 4) + 3];

        if (hitColorA != 0)
            return 0;

        lockMaskPixels = new byte[texWidth * texHeight * 4];
        Queue<int> fillPointX = new Queue<int>();
        Queue<int> fillPointY = new Queue<int>();
        fillPointX.Enqueue(x);
        fillPointY.Enqueue(y);

        int ptsx = 0, ptsy = 0;
        int pixel = 0;

        basePixel = (texWidth * (y) + x) * 4;

        int PixelCount = 0;
        while (fillPointX.Count > 0)
        {
            ptsx = fillPointX.Dequeue();
            ptsy = fillPointY.Dequeue();

            if (ptsy - 1 > -1)
            {
                pixel = (texWidth * (ptsy - 1) + ptsx) * 4; // down
                if (lockMaskPixels[pixel] == 0
                    && CompareThreshold(maskPixels[pixel + 0], hitColorR)
                    && CompareThreshold(maskPixels[pixel + 1], hitColorG)
                    && CompareThreshold(maskPixels[pixel + 2], hitColorB)
                    && CompareThreshold(maskPixels[pixel + 3], hitColorA))
                {
                    fillPointX.Enqueue(ptsx);
                    fillPointY.Enqueue(ptsy - 1);
                    lockMaskPixels[pixel] = 1;
                    if (calOnly)
                    {
                        pixels_of_current_area.Add(pixel);
                        PixelCount++;
                    }
                }
            }

            if (ptsx + 1 < texWidth)
            {
                pixel = (texWidth * ptsy + ptsx + 1) * 4; // right
                if (lockMaskPixels[pixel] == 0
                    && CompareThreshold(maskPixels[pixel + 0], hitColorR)
                    && CompareThreshold(maskPixels[pixel + 1], hitColorG)
                    && CompareThreshold(maskPixels[pixel + 2], hitColorB)
                    && CompareThreshold(maskPixels[pixel + 3], hitColorA))
                {
                    fillPointX.Enqueue(ptsx + 1);
                    fillPointY.Enqueue(ptsy);
                    lockMaskPixels[pixel] = 1;
                    if (calOnly)
                    {
                        pixels_of_current_area.Add(pixel);
                        PixelCount++;
                    }
                }
            }

            if (ptsx - 1 > -1)
            {
                pixel = (texWidth * ptsy + ptsx - 1) * 4; // left
                if (lockMaskPixels[pixel] == 0
                    && CompareThreshold(maskPixels[pixel + 0], hitColorR)
                    && CompareThreshold(maskPixels[pixel + 1], hitColorG)
                    && CompareThreshold(maskPixels[pixel + 2], hitColorB)
                    && CompareThreshold(maskPixels[pixel + 3], hitColorA))
                {
                    fillPointX.Enqueue(ptsx - 1);
                    fillPointY.Enqueue(ptsy);
                    lockMaskPixels[pixel] = 1;
                    if (calOnly)
                    {
                        pixels_of_current_area.Add(pixel);
                        PixelCount++;
                    }
                }
            }

            if (ptsy + 1 < texHeight)
            {
                pixel = (texWidth * (ptsy + 1) + ptsx) * 4; // up
                if (lockMaskPixels[pixel] == 0
                    && CompareThreshold(maskPixels[pixel + 0], hitColorR)
                    && CompareThreshold(maskPixels[pixel + 1], hitColorG)
                    && CompareThreshold(maskPixels[pixel + 2], hitColorB)
                    && CompareThreshold(maskPixels[pixel + 3], hitColorA))
                {
                    fillPointX.Enqueue(ptsx);
                    fillPointY.Enqueue(ptsy + 1);
                    lockMaskPixels[pixel] = 1;
                    if (calOnly)
                    {
                        pixels_of_current_area.Add(pixel);
                        PixelCount++;
                    }
                }
            }
        }
        return PixelCount;
    }

    public void DrawCircle(int x, int y)
    {
        int pixel = 0;

        for (int i = 0; i < brushSizeX4; i++)
        {
            int tx = (i % brushSizeX1) - brushSize;
            int ty = (i / brushSizeX1) - brushSize;

            if (tx * tx + ty * ty > brushSizeXbrushSize) continue;
            if (x + tx < 0 || y + ty < 0 || x + tx >= texWidth || y + ty >= texHeight) continue; // temporary fix for corner painting

            pixel = (texWidth * (y + ty) + x + tx) << 2;

            //if (lockMaskPixels[pixel] == 1)
            //{
            DrawPoint(pixel);
            //}
        } // for area
    } // DrawCircle()

    public void DrawLine(int startX, int startY, int endX, int endY)
    {
        int x1 = endX;
        int y1 = endY;
        int tempVal = x1 - startX;
        int dx = (tempVal + (tempVal >> 31)) ^ (tempVal >> 31); // http://stackoverflow.com/questions/6114099/fast-integer-abs-function
        tempVal = y1 - startY;
        int dy = (tempVal + (tempVal >> 31)) ^ (tempVal >> 31);


        int sx = startX < x1 ? 1 : -1;
        int sy = startY < y1 ? 1 : -1;
        int err = dx - dy;
        int pixelCount = 0;
        int e2;
        for (; ; ) // endless loop
        {
            pixelCount++;
            if (pixelCount > brushSizeDiv4) // might have small gaps if this is used, but its alot(tm) faster to skip few pixels
            {
                pixelCount = 0;
                DrawCircle(startX, startY);
            }

            if (startX == x1 && startY == y1) break;
            e2 = 2 * err;
            if (e2 > -dy)
            {
                err = err - dy;
                startX = startX + sx;
            }
            else if (e2 < dx)
            {
                err = err + dx;
                startY = startY + sy;
            }
        }
    } // drawline

    byte ByteLerp(byte value1, byte value2, float amount)
    {
        return (byte)(value1 + (value2 - value1) * amount);
    }

    public Texture2D getColorTexture()
    {
        return tex;
    }

    public Texture2D getOverlayTexture()
    {
        return overlayTex;
    }

    public void DoUndo()
    {
        if (saveLastState)
        {
            // saving last state
            undoPixels = new byte[texWidth * texHeight * 4];
            if (isBrushTool)
            {
                System.Array.Copy(overlayPixels, undoPixels, pixels.Length);
            }
            else
            {
                System.Array.Copy(pixels, undoPixels, pixels.Length);
            }
            pixelList.Add(undoPixels);
            pixelListTrack.Add(isBrushTool);
            if (pixelList.Count > limit)
            {
                pixelList.RemoveAt(0);
                pixelListTrack.RemoveAt(0);
            }
            saveLastState = false;
        }
        canRedoLastPixels = true;
        if (currentIndex > 0)
        {
            currentIndex--;
            if (pixelListTrack[currentIndex])
            {
                System.Array.Copy(pixelList[currentIndex], overlayPixels, undoPixels.Length);
                overlayTex.LoadRawTextureData(overlayPixels);
                overlayTex.Apply(false);
            }
            else
            {
                System.Array.Copy(pixelList[currentIndex], pixels, undoPixels.Length);
                tex.LoadRawTextureData(pixels);
                tex.Apply(false);
            }
        }
    }

    public void DoReDO()
    {
        if (currentIndex < pixelList.Count - 1)
        {
            currentIndex++;
            if (pixelListTrack[currentIndex])
            {
                System.Array.Copy(pixelList[currentIndex], overlayPixels, undoPixels.Length);
                overlayTex.LoadRawTextureData(overlayPixels);
                overlayTex.Apply(false);
            }
            else
            {
                System.Array.Copy(pixelList[currentIndex], pixels, undoPixels.Length);
                tex.LoadRawTextureData(pixels);
                tex.Apply(false);
            }
        }
        else
        {
            if (canRedoLastPixels == true)
            {
                try
                {
                    if (isBrushTool)
                    {
                        System.Array.Copy(redoPixels, overlayPixels, undoPixels.Length);
                        overlayTex.LoadRawTextureData(overlayPixels);
                        overlayTex.Apply(false);
                    }
                    else
                    {
                        System.Array.Copy(redoPixels, pixels, undoPixels.Length);
                        tex.LoadRawTextureData(pixels);
                        tex.Apply(false);
                    }
                    canRedoLastPixels = false;
                    currentIndex++;
                }
                catch (System.Exception ex)
                {

                }
            }
        }
    }

    public void savePixelsForRedoUndo()
    {
        canRedoLastPixels = false;
        while (1 != 2)
        {
            if (pixelList.Count != currentIndex)
            {
                if (pixelList.Count >= 1)
                {
                    pixelList.RemoveAt(pixelList.Count - 1);
                    pixelListTrack.RemoveAt(pixelListTrack.Count - 1);
                }
            }
            else
            {
                saveLastState = true;
                break;
            }
        }

        undoPixels = new byte[texWidth * texHeight * 4];
        if (isBrushTool)
        {
            System.Array.Copy(overlayPixels, undoPixels, pixels.Length);
        }
        else
        {
            System.Array.Copy(pixels, undoPixels, pixels.Length);
        }
        pixelList.Add(undoPixels);
        pixelListTrack.Add(isBrushTool);
        if (pixelList.Count > limit)
        {
            pixelList.RemoveAt(0);
            pixelListTrack.RemoveAt(0);
        }
        currentIndex = pixelList.Count;
    }

    public void colorOutline(Color colorToUse)
    {
        int pixel = 0;
        for (int y = 0; y < texHeight; y++)
        {
            for (int x = 0; x < texWidth; x++)
            {
                if (maskEffectPixels[pixel + 3] != 0)
                {
                    maskEffectPixels[pixel] = (byte)(colorToUse.r * 255);
                    maskEffectPixels[pixel + 1] = (byte)(colorToUse.g * 255);
                    maskEffectPixels[pixel + 2] = (byte)(colorToUse.b * 255);
                }
                pixel += 4;
            }
        }

        maskTex.LoadRawTextureData(maskEffectPixels);
        maskTex.Apply(false);
    }

    public void resetDrawingToDefault()
    {
        maskTex.LoadRawTextureData(maskPixels);
        maskTex.Apply(false);
        tex.LoadRawTextureData(pixels);
        tex.Apply(false);
    }

    public void setBrush(int brushIndex)
    {
        switch (brushIndex)
        {
            case 0:
                drawMode = DrawMode.Marker;
                break;
            case 1:
                drawMode = DrawMode.Alpha;
                break;
            case 2:
                drawMode = DrawMode.Solid;
                break;
            case 3:
                drawMode = DrawMode.Gredient;
                break;
            case 4:
                drawMode = DrawMode.Eraser;
                break;
            case 5:
                drawMode = DrawMode.Crayon;
                break;
            case 6:
                drawMode = DrawMode.Pencil;
                break;
            case 7:
                drawMode = DrawMode.Spray;
                break;
        }
        setPaintColor(localColor);
    }

    public void setPaintColor(Color newColor)
    {
        localColor = newColor;
        if (drawMode == DrawMode.Solid || drawMode == DrawMode.Gredient)
        {
            isBrushTool = false;
            brushSlider.gameObject.SetActive(false);
        }
        else
        {
            isBrushTool = true;
            //if (drawMode != DrawMode.Spray)
            //{
            brushSlider.gameObject.SetActive(true);
            brushSize = (int)brushSlider.value;
            //}
            //else
            //{
            //  brushSize = 12;
            //}
        }
        if (drawMode == DrawMode.Eraser)
        {
            paintColor = new Color(1f, 1f, 1f, 0f);
        }
        else
        {
            paintColor = localColor;
        }
        isGredient = drawMode == DrawMode.Gredient;
        WheelManager.instance.updateWheelsUi(isGredient);
        updateBrushSize();
    }

    public void applyEffect(int index)
    {
        if (index == 0)
        {
            pixelsUpdated = false;
            tex.LoadRawTextureData(pixels);
        }
        else
        {
            if (!pixelsUpdated)
            {
                patternPixels = new byte[texWidth * texHeight * 4];
                System.Array.Copy(pixels, patternPixels, pixels.Length);

                int pixel = 0;
                for (int y = 0; y < texHeight; y++)
                {
                    for (int x = 0; x < texWidth; x++)
                    {
                        if (patternPixels[pixel + 3] == 0)
                        {
                            patternPixels[pixel + 3] = 210;
                        }
                        else
                        {
                            if (patternPixels[pixel + 3] == 255)
                            {
                                patternPixels[pixel + 3] = 180;
                            }
                        }
                        pixel += 4;
                    }
                }
                tex.LoadRawTextureData(patternPixels);
                pixelsUpdated = true;
            }
        }
        tex.Apply(false);
    }

    private void Color32ArrayToByteArray(ref byte[] pixels, Texture2D tex)
    {
        Color32[] colors = tex.GetPixels32();
        int lengthOfColor32 = Marshal.SizeOf(typeof(Color32));
        int length = lengthOfColor32 * colors.Length;
        pixels = new byte[length];

        GCHandle handle = default(GCHandle);
        try
        {
            handle = GCHandle.Alloc(colors, GCHandleType.Pinned);
            IntPtr ptr = handle.AddrOfPinnedObject();
            Marshal.Copy(ptr, pixels, 0, length);
        }
        finally
        {
            if (handle != default(GCHandle))
                handle.Free();
        }
    }

    public void brushSizeChanged()
    {
        brushSize = (int)brushSlider.value;
        updateBrushSize();
    }

    void updateBrushSize()
    {
        brushSizeX1 = brushSize << 1;
        brushSizeXbrushSize = brushSize * brushSize;
        brushSizeX4 = brushSizeXbrushSize << 2;
        brushSizeDiv4 = brushSize >> 2;
    }
}
// class
