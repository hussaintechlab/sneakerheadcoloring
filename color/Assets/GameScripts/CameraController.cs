﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    bool firsttime;
    public Transform DrawingCanvas, ScaleAble, Bounds;
    public Vector3 lerpPan;
    public float lerpZoom;
    float maxZoom, w, h;

    RectTransform ScreenBottomLeft, ScreenUpperLeft, ScreenUpperRight, ScreenBottomRight;
    Vector3 CamLowerLeft, CamUpperLeft, CamUpperRight, CamLowerRight, LastPos;

    Vector3 startPosition;

    Vector3 startZoomOutPosition;
    Vector3 startZoomInPosition;
    Vector3 cameraLerpZoomOutPosition;
    Vector3 pinchMidPoint;

    Camera cam;
    float minZoom;
    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
        w = Screen.width / 2f;
        //h = cam.orthographicSize / 2f;
        h = w;
        startPosition = Painting.instance.transform.position;
        minZoom = 150f;
        ScreenBottomLeft = Bounds.GetChild(0).GetComponent<RectTransform>();
        ScreenUpperLeft = Bounds.GetChild(1).GetComponent<RectTransform>();
        ScreenUpperRight = Bounds.GetChild(2).GetComponent<RectTransform>();
        ScreenBottomRight = Bounds.GetChild(3).GetComponent<RectTransform>();

        lerpPan = DrawingCanvas.position;
        lerpZoom = cam.orthographicSize;
        maxZoom = lerpZoom;
        ScaleAble.GetChild(0).transform.position = new Vector3(-w, -h + Painting.instance.transform.position.y, 0f);
        ScaleAble.GetChild(1).transform.position = new Vector3(-w, h + Painting.instance.transform.position.y, 0f);
        ScaleAble.GetChild(2).transform.position = new Vector3(w, h + Painting.instance.transform.position.y, 0f);
        ScaleAble.GetChild(3).transform.position = new Vector3(w, -h + Painting.instance.transform.position.y, 0f);
        ScreenBottomLeft.position = Camera.main.WorldToScreenPoint(ScaleAble.GetChild(0).transform.position);
        ScreenUpperLeft.position = Camera.main.WorldToScreenPoint(ScaleAble.GetChild(1).transform.position);
        ScreenUpperRight.position = Camera.main.WorldToScreenPoint(ScaleAble.GetChild(2).transform.position);
        ScreenBottomRight.position = Camera.main.WorldToScreenPoint(ScaleAble.GetChild(3).transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        //ScreenBottomLeft.position = cam.WorldToScreenPoint(ScaleAble.GetChild(0).transform.position);
        //ScreenUpperLeft.position = cam.WorldToScreenPoint(ScaleAble.GetChild(1).transform.position);
        //ScreenUpperRight.position = cam.WorldToScreenPoint(ScaleAble.GetChild(2).transform.position);
        //ScreenBottomRight.position = cam.WorldToScreenPoint(ScaleAble.GetChild(3).transform.position);

        if (EventSystem.current.currentSelectedGameObject != null)
        {
            return;
        }

        CamLowerLeft = cam.ScreenToWorldPoint(ScreenBottomLeft.position);
        CamUpperLeft = cam.ScreenToWorldPoint(ScreenUpperLeft.position);
        CamUpperRight = cam.ScreenToWorldPoint(ScreenUpperRight.position);
        CamLowerRight = cam.ScreenToWorldPoint(ScreenBottomRight.position);


        //&&
        //      ((Input.GetTouch(0).deltaPosition.x > 0 && Input.GetTouch(1).deltaPosition.x > 0) ||
        //      (Input.GetTouch(0).deltaPosition.x < 0 && Input.GetTouch(1).deltaPosition.x < 0) ||
        //      (Input.GetTouch(0).deltaPosition.y > 0 && Input.GetTouch(1).deltaPosition.y > 0) ||
        //      (Input.GetTouch(0).deltaPosition.y < 0 && Input.GetTouch(1).deltaPosition.y < 0))
        if (Input.touchCount == 2)
        {
            if (
                (Input.GetTouch(0).deltaPosition.x > 0 && Input.GetTouch(1).deltaPosition.x < 0) ||
                (Input.GetTouch(0).deltaPosition.x < 0 && Input.GetTouch(1).deltaPosition.x > 0) ||
                (Input.GetTouch(0).deltaPosition.y > 0 && Input.GetTouch(1).deltaPosition.y < 0) ||
                (Input.GetTouch(0).deltaPosition.y < 0 && Input.GetTouch(1).deltaPosition.y > 0)
                )
            {
                firsttime = true;
                // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                // ... change the orthographic size based on the change in distance between the touches.
                lerpZoom += (deltaMagnitudeDiff * 2f);
                lerpZoom = Mathf.Clamp(lerpZoom, minZoom, maxZoom);

                if (cameraLerpZoomOutPosition == Vector3.zero)
                {
                    pinchMidPoint = (transform.GetComponent<Camera>().ScreenToWorldPoint(Input.GetTouch(0).position) + transform.GetComponent<Camera>().ScreenToWorldPoint(Input.GetTouch(1).position)) / 2;
                    cameraLerpZoomOutPosition = new Vector3(pinchMidPoint.x, pinchMidPoint.y, transform.position.z);
                    startZoomOutPosition = transform.position;
                }
                //// Zoom out
                if (deltaMagnitudeDiff > 0)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(0, 0, -11), Mathf.Abs(Vector3.Distance(startZoomInPosition, new Vector3(0, 0, -11)) * ((cam.orthographicSize - lerpZoom) / (maxZoom - minZoom))));

                    pinchMidPoint = (transform.GetComponent<Camera>().ScreenToWorldPoint(Input.GetTouch(0).position) + transform.GetComponent<Camera>().ScreenToWorldPoint(Input.GetTouch(1).position)) / 2;
                    cameraLerpZoomOutPosition = new Vector3(pinchMidPoint.x, pinchMidPoint.y, transform.position.z);
                    startZoomOutPosition = transform.position;
                }
                ////Zoom in
                else if (deltaMagnitudeDiff < 0)
                {
                    transform.position = Vector3.MoveTowards(transform.position, cameraLerpZoomOutPosition, Mathf.Abs(Vector3.Distance(startZoomOutPosition, cameraLerpZoomOutPosition) * ((cam.orthographicSize - lerpZoom) / (maxZoom - minZoom))));

                    startZoomInPosition = transform.position;
                }

                if (lerpPan.x + w < CamLowerRight.x)
                {
                    lerpPan += new Vector3((Mathf.Abs(Mathf.Abs(lerpPan.x + w) - Mathf.Abs(CamLowerRight.x))), 0, 0);
                }
                if (lerpPan.x - w > CamLowerLeft.x)
                {
                    lerpPan -= new Vector3((Mathf.Abs(Mathf.Abs(lerpPan.x - w) - Mathf.Abs(CamLowerLeft.x))), 0, 0);
                }
                if (lerpPan.y + h < CamUpperRight.y)
                {
                    lerpPan += new Vector3(0, (Mathf.Abs(Mathf.Abs(lerpPan.y + h) - Mathf.Abs(CamUpperRight.y))), 0);
                }
                if (lerpPan.y - h > CamLowerLeft.y)
                {
                    lerpPan -= new Vector3(0, (Mathf.Abs(Mathf.Abs(lerpPan.y - h) - Mathf.Abs(CamLowerLeft.y))), 0);
                }
            }
            else
            {
                if (Input.touchCount == 2 && (firsttime || Input.GetTouch(1).phase == TouchPhase.Began))
                {
                    firsttime = false;
                    LastPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                }
                else
                {
                    float deltaX = (cam.ScreenToWorldPoint(Input.GetTouch(0).position).x) - LastPos.x;
                    float deltaY = (cam.ScreenToWorldPoint(Input.GetTouch(0).position).y) - LastPos.y;

                    if (lerpPan.x + w + (deltaX * 2f) >= CamLowerRight.x &&
                           lerpPan.x - w + (deltaX * 2f) <= CamLowerLeft.x)
                    {
                        lerpPan += new Vector3(deltaX * 2f, 0, 0);
                    }
                    if (lerpPan.y + h + (deltaY * 2f) >= CamUpperLeft.y &&
                          lerpPan.y - h + (deltaY * 2f) <= CamLowerLeft.y)
                    {
                        lerpPan += new Vector3(0, deltaY * 2f, 0);
                    }
                    LastPos = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
                }
            }
        }
        cam.orthographicSize = lerpZoom;
        DrawingCanvas.position = lerpPan;

        if (Input.touchCount < 2)
        {
            cameraLerpZoomOutPosition = Vector3.zero;
        }
            
    }
}
