﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour
{
    public Sprite iPhone, iPad;
    public Image splash;

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        splash.sprite = iPhone;
#elif UNITY_IOS
        var identifier = SystemInfo.deviceModel;
        if (identifier.StartsWith("iPhone"))
        {
            splash.sprite = iPhone;
        }
        else if (identifier.StartsWith("iPad"))
        {
            splash.sprite = iPad;
        }
#endif
        StartCoroutine(loadScene());
    }

    IEnumerator loadScene()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadSceneAsync("Main");
    }
}
