﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.IO;
using LitJson;
// using UnityEngine.iOS;

public class RegionsManager : MonoBehaviour
{
    public Image LoadingBG;
    int texWidth, selectedBrushIndex = 2;

    Painting mPaintInstance;
    Vector3 camDefaultPos;
    float camDefaultSize;
    Texture2D combinedTexture;
    bool isComplete = false, waitForOther = false;
    RenderTexture viewRender;

    public Camera render;
    public Image loading, undo, redo;
    public RectTransform colorCanvas;
    public GameObject brushes, completePanel, loadingPanel, toast, outlineBtnPrefab, wheelPanel;
    public Sprite[] undoStates;
    public Sprite[] redoStates;
    public RawImage drawing, outline, pattern, overlay, rendererImg;
    public Transform outlinesContent, effectsContent, effectBtn, outlineBtn;
    public Transform allBrushes;
    public Sprite[] allBrushesIcons;
    public Image currentBrush;
    public Color selectedBrushColor, highlightColor;
    public static RegionsManager instance;
    public LoopScrollRect wheelLooper;
    public Transform[] disablers;
    public Transform top, bottom;
    public GameObject waterMark;

    void Awake()
    {
        
        instance = this;
        //DownloadManager.StopDownlodingManager();
        GameConfig.backColored = false;
        GameConfig.frontColored = false;
        float width = drawing.transform.parent.GetComponent<RectTransform>().rect.width;
        drawing.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(-10, width);
        render.orthographicSize = Screen.width / 2;
        int appRun = PlayerPrefs.GetInt("appRun", 1);
        if (appRun % 3 == 0)
        {
            // Device.RequestStoreReview();
        }
        PlayerPrefs.SetInt("appRun", appRun + 1);
        if (GameConfig.GetWeekSubscription() == 1)
        {
            waterMark.SetActive(false);
        }
       
    }

    void Start()
    {
         
        Application.targetFrameRate = 300;
        mPaintInstance = transform.GetComponent<Painting>();
        GameConfig.zoomPanEnable = true;
        camDefaultPos = Camera.main.transform.position;
        camDefaultSize = Camera.main.orthographicSize;
        texWidth = GetComponent<Renderer>().material.GetTexture("_MaskTex").width;
        combinedTexture = new Texture2D(texWidth, texWidth);

        viewRender = new RenderTexture(1024, 1024, 32, RenderTextureFormat.ARGB32);
        render.targetTexture = viewRender;

        string outlinesJString = Resources.Load<TextAsset>("outlines").text;
        JsonData outlinesJson = JsonMapper.ToObject(outlinesJString);
        for (var i = 0; i < outlinesJson.Count; i++)
        {
            var outlineBtn = Instantiate(outlineBtnPrefab, outlinesContent);
            outlineBtn.name = i.ToString();
            Color outlineColor;
            if (ColorUtility.TryParseHtmlString((string)outlinesJson[i]["color"], out outlineColor))
            {
                outlineBtn.GetComponent<Image>().color = outlineColor;
            }
            outlineBtn.transform.GetChild(0).GetComponent<Text>().text = (string)outlinesJson[i]["title"];
            outlineBtn.GetComponent<Button>().onClick.AddListener(OnOutlineClick);
        }
        for (var i = 0; i < GameConfig.Effects.Count; i++)
        {
            var outlineBtn = Instantiate(outlineBtnPrefab, effectsContent);
            outlineBtn.name = i.ToString();
            outlineBtn.GetComponent<Image>().sprite = Resources.Load("EffectIcons/" + GameConfig.Effects[i], typeof(Sprite)) as Sprite;
            outlineBtn.transform.GetChild(0).GetComponent<Text>().text = GameConfig.Effects[i];
            outlineBtn.GetComponent<Button>().onClick.AddListener(onEffectClick);
        }
        allBrushes.GetChild(selectedBrushIndex).GetComponent<Image>().color = selectedBrushColor;
        currentBrush.sprite = allBrushesIcons[selectedBrushIndex];
        resetFinalUI(true);
        rendererImg.texture = viewRender;
        Invoke("setCanvasMode",0.01f);
        
    }
    void setCanvasMode()
    {
        waterMark.transform.parent.GetComponent<Canvas>().gameObject.SetActive(false);
        waterMark.transform.parent.GetComponent<Canvas>().gameObject.SetActive(true);
        waterMark.transform.parent.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;

    }

    void OnScreenshotSaved(string status)
    {
        StartCoroutine(showToast("Image saved in Phone Gallery"));
    }

    void moveCamera(Vector3 targetPosition, float targetZoom)
    {
        GameConfig.zoomPanEnable = false;
        StartCoroutine(MoveOverSpeed(Camera.main.transform, targetPosition, 750f));
        StartCoroutine(MoveOverSize(new Vector3(Camera.main.orthographicSize, 0f, 0f), new Vector3(targetZoom, 0f, 0f), 750f));
    }

    void getReadyForLoadScene()
    {
        loading.fillAmount = 0f;
        if (GameConfig.IsAllPicturesUnlocked() == 1 || GameConfig.GetWeekSubscription() == 1)
        {
            loadingPanel.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
        }
        loadingPanel.SetActive(true);
    }

    void panelAction(bool status)
    {
        GameConfig.zoomPanEnable = status;
        mPaintInstance.enableMouse = status;
    }

    public void onShareDownloadClick(int mode)
    {
        Debug.LogError(mode);
        if (mode == 0)
        {
            Debug.LogError("0");
            GetComponent<Share>().ShareScreenshotWithText(combinedTexture);
        }
        else
        {
            Debug.LogError("Else");
            Debug.Log("Permission result: " + NativeGallery.SaveImageToGallery(combinedTexture.EncodeToPNG(), Application.productName.Replace(" ", ""), System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".png", null));
            OnScreenshotSaved("");
        }
    }

    public void onBackClick()
    {
        colorCanvas.gameObject.SetActive(false);
        if( waterMark.transform.parent.GetComponent<Canvas>().enabled){
         waterMark.transform.parent.GetComponent<Canvas>().gameObject.SetActive(false);
        }
        getReadyForLoadScene();
        if (GameConfig.backColored || GameConfig.frontColored)
        {
            GameConfig.setRegionProgress(GameConfig.folderName + GameConfig.regionNumber + "isEdited");
            moveCamera(camDefaultPos, camDefaultSize);
        }
        else
        {
            StartCoroutine(loadScene());
        }
    }

    public void loadMainScene()
    {
        getReadyForLoadScene();
        StartCoroutine(loadScene());
    }

    IEnumerator hideToast(float delay = 1.5f)
    {
        yield return new WaitForSeconds(delay);
        toast.SetActive(false);
    }

    IEnumerator showToast(string msg)
    {
        yield return new WaitForEndOfFrame();
        toast.transform.GetChild(0).GetComponent<Text>().text = msg;
        toast.SetActive(true);
        StartCoroutine(hideToast());
    }


    IEnumerator MoveOverSpeed(Transform objectToMove, Vector3 end, float speed)
    {

        // speed should be 1 unit per second
        while (objectToMove.position != end)
        {
            objectToMove.position = Vector3.MoveTowards(objectToMove.position, end, speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        waitForOther = true;
    }

    IEnumerator MoveOverSize(Vector3 target, Vector3 end, float speed)
    {
        // speed should be 1 unit per second
        while (target != end)
        {
            target = Vector3.MoveTowards(target, end, speed * Time.deltaTime);
            Camera.main.orthographicSize = target.x;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitUntil(() => waitForOther == true ? true : false);
        waitForOther = false;
        if (isComplete || loadingPanel.activeSelf)
        {
            StartCoroutine(combineTextures());
        }
        else
        {
            GameConfig.zoomPanEnable = true;
        }
    }

    IEnumerator loadScene()
    {
        loading.fillAmount = 0f;
        AsyncOperation operation = SceneManager.LoadSceneAsync("Main");
        while (!operation.isDone)
        {
            loading.fillAmount = Mathf.Clamp01(operation.progress / 0.9f);
            yield return null;
        }
    }

    IEnumerator combineTextures()
    {
        yield return new WaitForSeconds(0.01f);
        // Capturing Large texture for share,download
        getRenderer();
        //Saving Large Image
        DirectoryInfo directory = new DirectoryInfo(GameConfig.dataPath() + "/Back");
        if (!directory.Exists)
        {
            directory.Create();
        }
        if (GameConfig.backColored)
        {
            File.WriteAllBytes(GameConfig.dataPath() + "/Back/" + GameConfig.folderName + "_" + GameConfig.regionNumber + ".png", mPaintInstance.getColorTexture().EncodeToPNG());
        }
        //Saving Overlay Image
        directory = new DirectoryInfo(GameConfig.dataPath() + "/Overlay");
        if (!directory.Exists)
        {
            directory.Create();
        }
        if (GameConfig.frontColored)
        {
            File.WriteAllBytes(GameConfig.dataPath() + "/Overlay/" + GameConfig.folderName + "_" + GameConfig.regionNumber + ".png", mPaintInstance.getOverlayTexture().EncodeToPNG());
        }

        // Small Icon
        RenderTexture rt = RenderTexture.GetTemporary(256, 256);
        RenderTexture.active = rt;
        Graphics.Blit(combinedTexture, rt);
        Texture2D nTex = new Texture2D(256, 256);
        nTex.ReadPixels(new Rect(0, 0, 256, 256), 0, 0);
        nTex.Apply();
        RenderTexture.active = null;
        rt.Release();
        if (GameConfig.isWorks)
        {
            if (GameConfig.categoryIndex == GameConfig.workCatIndex)
            {
                GameConfig.regionIcons[GameConfig.regionNumber - 1] = nTex;
            }
        }
        else
        {
            GameConfig.regionIcons[GameConfig.regionNumber - 1] = nTex;
        }
        //Saving Icon
        directory = new DirectoryInfo(GameConfig.regionIconPath());
        if (!directory.Exists)
        {
            directory.Create();
        }
        File.WriteAllBytes(GameConfig.regionIconPath() + "/" + GameConfig.regionNumber + ".png", nTex.EncodeToPNG());
        //Saving Work Icon
        directory = new DirectoryInfo(GameConfig.dataPath() + "/Icon");
        if (!directory.Exists)
        {
            directory.Create();
        }
        string worksName = GameConfig.folderName + "_" + GameConfig.regionNumber + ".png";
        bool found = false;
        int index = 0;
        for (; index < GameConfig.workIcons.Count; index++)
        {
            if (GameConfig.workIcons[index].name == worksName)
            {
                found = true;
                break;
            }
        }
        if (found)
        {
            GameConfig.workIcons.RemoveAt(index);
        }
        nTex.name = worksName;
        GameConfig.workIcons.Insert(0, nTex);
        File.WriteAllBytes(GameConfig.dataPath() + "/Icon/" + GameConfig.folderName + "_" + GameConfig.regionNumber + ".png", nTex.EncodeToPNG());
        if (!loadingPanel.activeSelf)
        {
            yield return new WaitForSeconds(0.5f);
            completePanel.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            completePanel.transform.GetChild(0).GetComponent<RawImage>().texture = viewRender;
            iTween.ScaleTo(completePanel.transform.GetChild(0).gameObject, Vector3.one, 2f);
        }
        else
        {
            StartCoroutine(loadScene());
        }
    }

    public void updateUndoUI(int index)
    {
        undo.sprite = undoStates[index];
    }

    public void updateRedoUI(int index)
    {
        redo.sprite = redoStates[index];
    }

    public void goForEffects()
    {
        mPaintInstance.pixelsUpdated = false;
        pattern.texture = null;
        mPaintInstance.patternMesh.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", null);
        resetFinalUI(true);
        mPaintInstance.enabled = false;
        colorCanvas.gameObject.SetActive(false);
        mPaintInstance.patternMesh.SetActive(true);
        completePanel.SetActive(true);
    }

    void OnOutlineClick()
    {
        mPaintInstance.colorOutline(EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color);
    }

    void onEffectClick()
    {
        var selected = EventSystem.current.currentSelectedGameObject;
        int index = int.Parse(selected.name);
        if (index == 0)
        {
            mPaintInstance.patternMesh.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", null);
            //pattern.texture = null;
        }
        else
        {
            mPaintInstance.patternMesh.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", Resources.Load("Effects/" + GameConfig.Effects[index]) as Texture2D);
            //pattern.texture = Resources.Load("Effects/" + GameConfig.Effects[index]) as Texture2D;
        }
        mPaintInstance.applyEffect(index);
    }

    public void OnBackToDrawingClick()
    {
        mPaintInstance.enabled = true;
        completePanel.SetActive(false);
        mPaintInstance.patternMesh.SetActive(false);
        colorCanvas.gameObject.SetActive(true);
        mPaintInstance.resetDrawingToDefault();
        WheelManager.instance.WheelRefresh();
    }

    public void onMenuClick()
    {
        brushes.SetActive(!brushes.activeSelf);
    }

    public void onBrushClick()
    {
        GameObject clickedBrush = EventSystem.current.currentSelectedGameObject;
        int clickedIndex = clickedBrush.transform.GetSiblingIndex();
        if (clickedIndex != selectedBrushIndex)
        {
            allBrushes.GetChild(selectedBrushIndex).GetComponent<Image>().color = Color.white;
            selectedBrushIndex = clickedIndex;
            currentBrush.sprite = allBrushesIcons[selectedBrushIndex];
            clickedBrush.GetComponent<Image>().color = selectedBrushColor;
            onMenuClick();
            mPaintInstance.setBrush(selectedBrushIndex);
        }
    }

    public void wheelPanelOn()
    {
        mPaintInstance.enabled = false;
        for (var i = 0; i < wheelLooper.content.childCount; i++)
        {
            wheelLooper.content.GetChild(i).GetChild(2).GetComponent<Image>().enabled = Painting.instance.isGredient;
        }
        for (var i = 0; i < disablers.Length; i++)
        {
            disablers[i].gameObject.SetActive(false);
        }
        wheelPanel.SetActive(true);
    }

    public void wheelPanelOff()
    {
        mPaintInstance.enabled = true;
        for (var i = 0; i < disablers.Length; i++)
        {
            disablers[i].gameObject.SetActive(true);
        }
        wheelPanel.SetActive(false);
        WheelManager.instance.WheelRefresh();
    }

    void resetFinalUI(bool isEffect)
    {
        effectBtn.GetComponent<Image>().color = isEffect ? highlightColor : Color.clear;
        effectBtn.GetChild(0).GetComponent<Text>().color = isEffect ? Color.white : Color.black;
        outlineBtn.GetComponent<Image>().color = isEffect ? Color.clear : highlightColor;
        outlineBtn.GetChild(0).GetComponent<Text>().color = isEffect ? Color.black : Color.white;
        effectsContent.parent.gameObject.SetActive(isEffect);
        outlinesContent.parent.gameObject.SetActive(!isEffect);
    }

    public void onEffectBtnClick()
    {
        resetFinalUI(true);
    }

    public void onOutlineBtnClick()
    {
        resetFinalUI(false);
    }

    public void onDownloadClick()
    {
        getRenderer();
        //StartCoroutine(getRenderer());
        Debug.Log("Permission result: " + NativeGallery.SaveImageToGallery(combinedTexture, Application.productName.Replace(" ", ""), System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".png", null));
        OnScreenshotSaved("");
    }

    public void onShareClick()
    {
        getRenderer();
        //StartCoroutine(getRenderer());
        GetComponent<Share>().ShareScreenshotWithText(combinedTexture);
    }

    void getRenderer()
    {

        RenderTexture.active = viewRender;
        //yield return new WaitForEndOfFrame();
        combinedTexture.ReadPixels(new Rect(0, 0, texWidth, texWidth), 0, 0);
        combinedTexture.Apply();
        //yield return new WaitForEndOfFrame();
        RenderTexture.active = null;
    }
}
