using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class SoundCollider : MonoBehaviour
{
    public bool playOnActive;
    bool canPlay;
    private void OnTriggerEnter(Collider other)
    {
     MMVibrationManager.Haptic(HapticTypes.SoftImpact);
     Debug.Log("softimpact");
        if (other.tag == "soundCollider")
        {
            if (canPlay || playOnActive)
            {
                WheelManager.instance._sound.Play();
            }
            else
            {
                canPlay = true;
            }
        }
    }

    private void OnDisable()
    {
        if (playOnActive)
        {
            canPlay = true;
        }
        else
        {
            canPlay = false;
        }
    }
}
