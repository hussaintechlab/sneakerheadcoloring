using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollVelocity : MonoBehaviour,IEndDragHandler
{
    public ScrollRect scrollRect;
    public LoopVerticalScrollRect loopVerticalScrollRect;
    public float factor;
    public bool isHorizontal;
    bool canMultiply;

    // Start is called before the first frame update
    void Start()
    {
       
    }
    public void OnValueChange()
    {
        if (canMultiply)
        {
            if (isHorizontal)
            {
                if (scrollRect != null && factor > 0)
                    scrollRect.velocity = new Vector2(scrollRect.velocity.x * (factor), scrollRect.velocity.y);

                if (loopVerticalScrollRect != null && factor > 0)
                    loopVerticalScrollRect.velocity = new Vector2(loopVerticalScrollRect.velocity.x * (factor), loopVerticalScrollRect.velocity.y);
            }
            else
            {
                if (scrollRect != null && factor > 0)
                    scrollRect.velocity = new Vector2(scrollRect.velocity.x, scrollRect.velocity.y * (factor));

                if (loopVerticalScrollRect != null && factor > 0)
                    loopVerticalScrollRect.velocity = new Vector2(loopVerticalScrollRect.velocity.x, loopVerticalScrollRect.velocity.y * (factor));
            }
            canMultiply = false;
        }
    }

    public void OnEndDrag(PointerEventData data)
    {
        canMultiply = true;
    }
}


