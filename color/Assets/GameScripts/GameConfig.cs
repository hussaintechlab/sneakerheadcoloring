﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig : MonoBehaviour
{
    public static List<string> Effects = new List<string> { "No effect", "Brick Wall", "Concrete", "Crumpled Paper", "Knitted", "Old Wood", "Sand", "Soft Wood", "Watercolor" };
    public static List<Texture2D> regionIcons = null;
    public static List<Texture2D> workIcons = null;
    public static bool isInitilized = false;
    public static bool zoomPanEnable = true;
    public static string folderName = "Nike";
    public static int regionNumber = 1;
    public static int categoryIndex = 0;
    public static float categoriesScrollVal = 0f;
    public static float imagesScrollVal = 0f;
    public static int selectedCell = 0;
    public static int InternetTimeout = 15;
    public static int CurrentInterneTimeCheck = 0;
    public static bool backColored = false;
    public static bool frontColored = false;
    public static int InternetCheckCount = 0;
    public static int workCatIndex = -1;
    public static bool isWorks = false;

    public static void setRegionProgress(string regionKey, int value = 1)
    {
        PlayerPrefs.SetInt(regionKey, value);
    }

    public static int getRegionProgress(string regionKey)
    {
        return PlayerPrefs.GetInt(regionKey, 0);
    }

    public static string regionIconPath()
    {
        return Application.persistentDataPath + "/" + folderName;
    }

    public static string dataPath()
    {
        return Application.persistentDataPath + "/Data";
    }

    public static void SetUnlockAllPictures()
    {
        PlayerPrefs.SetInt("unlock_all_pictures", 1);
    }

    public static int IsAllPicturesUnlocked()
    {
        return PlayerPrefs.GetInt("unlock_all_pictures", 0);
    }
    public static void ResetWeekSubscription()
    {
        PlayerPrefs.SetInt("WeekSubscription", 0);
    }
    public static void SetWeekSubscription()
    {
        PlayerPrefs.SetInt("WeekSubscription", 1);
    }
    public static int GetWeekSubscription()
    {
        return PlayerPrefs.GetInt("WeekSubscription", 0);
    }
    public static void SetTodaysDownloadedImages(int folderIndex)
    {
        int images = GetTodaysDownloadedImages(folderIndex);
        images++;
        PlayerPrefs.SetInt(DateTime.Now.ToString("dd-MM-yyyy") + "_" + folderIndex, images);
    }

    public static int GetTodaysDownloadedImages(int folderIndex)
    {
        return PlayerPrefs.GetInt(DateTime.Now.ToString("dd-MM-yyyy") + "_" + folderIndex, 0);
    }
}
