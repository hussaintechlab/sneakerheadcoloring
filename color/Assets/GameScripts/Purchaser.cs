﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class Purchaser : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;
    private static IStoreExtension _playStoreExtensions;
    public static Purchaser instance;

    //Subscription Premium
    public static string PremiumProductId = "WeekSubscription";
    // IOS
    private static string PremiumProductIdApple = "";
    // Google
    private static string PremiumProductIdGoogle = "";

    //Lifetime Premium
    public static string LifetimePremiumProductId = "LifetimeWeekSubscription";
    // IOS
    private static string LifetimePremiumProductIdApple = "";
    // Google
    private static string LifetimePremiumProductIdGoogle = "";

    void Start()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
        }
        instance = this;
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
    }
    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            return;
        }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(PremiumProductId, ProductType.Subscription, new IDs(){
                { PremiumProductIdApple, AppleAppStore.Name},
                { PremiumProductIdGoogle, GooglePlay.Name},
                });

        builder.AddProduct(LifetimePremiumProductId, ProductType.NonConsumable, new IDs(){
                { LifetimePremiumProductIdApple, AppleAppStore.Name},
                { LifetimePremiumProductIdGoogle, GooglePlay.Name},
                });

        UnityPurchasing.Initialize(this, builder);
    }
    private bool IsInitialized()
    {
        return m_StoreController != null && _playStoreExtensions != null;
    }
    public void SubscribeToPremium()
    {
        BuyProductID(PremiumProductId);
    }

    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }
    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }
    }
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        m_StoreController = controller;
        _playStoreExtensions = extensions.GetExtension<IStoreExtension>();
#if UNITY_ANDROID
        foreach (Product p in controller.products.all)
        {
            if (String.Equals(p.definition.id, PremiumProductId, StringComparison.Ordinal))
            {
                if (p.hasReceipt)
                {
                    Debug.Log("Subscription Active");
                    GameConfig.SetWeekSubscription();
                    FindObjectOfType<MainUiHandler>().onGetWeekPremium();
                }
                else
                {
                    Debug.Log("Subscription Expired");
                    GameConfig.ResetWeekSubscription();
                    FindObjectOfType<MainUiHandler>().adjustUINormal();
                }
            }
        }
#endif
#if UNITY_IOS
        string localsave = PlayerPrefs.GetString("Receipt", null);
        if (!String.IsNullOrEmpty(localsave))
        {
            var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                AppleTangle.Data(), Application.identifier);
            var localResult = validator.Validate(localsave);
            foreach (IPurchaseReceipt productReceipt in localResult)
            {
                AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
                if (null != apple)
                {

                    if (isSubscriptionActiveForApple(apple))
                    {
                        Debug.Log("Subscription Active");
                         GameConfig.SetWeekSubscription();
        FindObjectOfType<MainUiHandler>().onGetWeekPremium();
                    }
                    else
                    {
                        Debug.Log("Subscription Expired");
                         GameConfig.ResetWeekSubscription();
         FindObjectOfType<MainUiHandler>().adjustUINormal();
                    }
                }
            }
        }
#endif
    }
    public void OnInitializeFailed(InitializationFailureReason error)
    {

        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, PremiumProductId, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            PlayerPrefs.SetString("Receipt", args.purchasedProduct.receipt);
            PlayerPrefs.Save();
            var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                AppleTangle.Data(), Application.identifier);
            try
            {
                var result = validator.Validate(args.purchasedProduct.receipt);
                Debug.Log("Receipt is valid.");
                foreach (IPurchaseReceipt productReceipt in result)
                {
                    GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
                    if (null != google)
                    {
                        if (IsSubActiveForGoogle(google))
                        {
                            Debug.Log("Subscription Active");
                            GameConfig.SetWeekSubscription();
                            FindObjectOfType<MainUiHandler>().onGetWeekPremium();
                        }
                        else
                        {
                            Debug.Log("Subscription Expired");
                            GameConfig.ResetWeekSubscription();
                            FindObjectOfType<MainUiHandler>().adjustUINormal();
                        }
                    }

                    AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
                    if (null != apple)
                    {
                        if (isSubscriptionActiveForApple(apple))
                        {
                            Debug.Log("Subscription Active");
                            GameConfig.SetWeekSubscription();
                            FindObjectOfType<MainUiHandler>().onGetWeekPremium();
                        }
                        else
                        {
                            Debug.Log("Subscription Expired");
                            GameConfig.ResetWeekSubscription();
                            FindObjectOfType<MainUiHandler>().adjustUINormal();
                        }
                    }
                }
            }
            catch (IAPSecurityException)
            {
                Debug.Log("Invalid receipt, not unlocking content");
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, LifetimePremiumProductId, StringComparison.Ordinal))
        {
            FindObjectOfType<MainUiHandler>().onPurchaseUnlockAllPictures();
        }
        return PurchaseProcessingResult.Complete;
    }
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    public bool IsSubActiveForGoogle(GooglePlayReceipt googleReceipt)
    {
        bool isActive = false;
        GooglePlayReceipt google = googleReceipt;
        if (null != google)
        {
            if (google.purchaseState == GooglePurchaseState.Purchased)
            {
                isActive = true;
            }
        }
        return isActive;
    }
    public bool isSubscriptionActiveForApple(AppleInAppPurchaseReceipt appleReceipt)
    {
        bool isActive = false;
        AppleInAppPurchaseReceipt apple = appleReceipt;
        if (null != apple)
        {
            DateTime expirationDate = apple.subscriptionExpirationDate;
            DateTime now = DateTime.Now;
            if (DateTime.Compare(now, expirationDate) < 0)
            {
                isActive = true;
            }
        }
        return isActive;
    }

}
