﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.IO;
using Lean.Touch;
using EnhancedScrollerDemos.SuperSimpleDemo;
using EnhancedUI.EnhancedScroller;
using System.Linq;
// using UnityEngine.iOS;
using MoreMountains.NiceVibrations;

public class MainUiHandler : MonoBehaviour
{
    public static MainUiHandler Instance;
    public GameObject loadingPanel, buttonPrefab, categoreyPrefab, continuePanel, overlay, inAppPanel, unlockPanel, toast, PreviousPage, NextPage, freeTrialBar, settingsPanel;
    public Transform positionTaker;
    public Image loadProgress;
    public Transform categoriesContent;
    public List<string> category_names = new List<string>();
    public List<int> images_count = new List<int>();
    public GameObject InternetPanel;
    public Color selectedColor;
    public RectTransform bigScroll, scrollerContent;
    public RectTransform jugar;
    Vector3 continuePanelPos, inAppPanelPos;
    bool clickEnabled = true;
    public ScrollRect imagesScrollRect;
    GameObject selectedUI;
    public SimpleDemo _myDemo;
    private RectTransform _scroll;
    //Swipe Detection
    private Vector2 fingerDown;
    private Vector2 fingerUp;
    public float SWIPE_THRESHOLD = 50f;
    public Sprite[] sound;
    int status = 1;
    int selectedTab = 0;
    //
    public bool ispremium;
//
    void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start()
    {
        GameConfig.isWorks = false;
         ispremium =false;
        GameConfig.workCatIndex = -1;
        Application.targetFrameRate = 300;
        showHideUi(loadingPanel);
        loadProgress.fillAmount = 1f;
        GameConfig.folderName = category_names[GameConfig.categoryIndex];
        loadCategories();
        if (!GameConfig.isInitilized)
        {
            GameConfig.isInitilized = true;
            loadImages();
            showHideUi(loadingPanel);
            GameConfig.workIcons = new List<Texture2D>();
        }
        else
        {
            _myDemo.LoadLargeData();
            StartCoroutine(goToCell());
        }
        continuePanelPos = continuePanel.transform.position;
        inAppPanelPos = inAppPanel.transform.position;
        if (GameConfig.IsAllPicturesUnlocked() == 1 || GameConfig.GetWeekSubscription() == 1)
        {
            adjustUIPremium();
        }
        _scroll = imagesScrollRect.GetComponent<RectTransform>();
    }

    IEnumerator goToCell(bool loading = true)
    {
        yield return new WaitForSeconds(0.01f);
        _myDemo.scroller.JumpToDataIndex(GameConfig.selectedCell, 0f, 0, false, EnhancedScroller.TweenType.immediate, 0);
        categoriesContent.parent.GetComponent<ScrollRect>().horizontalNormalizedPosition = GameConfig.categoriesScrollVal;
        if (loading)
        {
            showHideUi(loadingPanel);
        }
    }
    //void Update()
    //{
    //bigScroll.offsetMax = new Vector2(0, Mathf.Clamp(bigScroll.offsetMax.y, 0, 500f));
    //if (Vector2.Distance(bigScroll.offsetMax, new Vector2(0f, 500f)) <= 0.1f)
    //{
    //    bigScroll.GetComponent<LeanDragTranslate>().enabled = false;
    //    _scroll.offsetMin = new Vector2(0, -bigScroll.offsetMin.y + 140f);
    //}
    //else
    //{
    //    bigScroll.GetComponent<LeanDragTranslate>().enabled = true;
    //    _scroll.offsetMin = Vector2.zero;
    //}

    //foreach (Touch touch in Input.touches)
    //{
    //    if (touch.phase == TouchPhase.Began)
    //    {
    //        fingerUp = touch.position;
    //        fingerDown = touch.position;
    //    }

    //    //Detects swipe after finger is released
    //    if (touch.phase == TouchPhase.Ended)
    //    {
    //        fingerDown = touch.position;
    //        checkSwipe();
    //    }
    //}

    //if (Input.GetMouseButtonUp(0))
    //{
    //    if (bigScroll.position.y < ((2300f / 2688f) * Screen.height + (1350f / 2688f) * Screen.height) / 2)
    //    {
    //        iTween.MoveTo(bigScroll.gameObject, iTween.Hash("position", new Vector3(bigScroll.position.x, (1350f / 2688f) * Screen.height, bigScroll.position.z), "speed", 500f, "EaseType", iTween.EaseType.easeInOutSine, "LoopType", iTween.LoopType.none));
    //    }
    //    else
    //    {
    //        iTween.MoveTo(bigScroll.gameObject, iTween.Hash("position", new Vector3(bigScroll.position.x, (2300f / 2688f) * Screen.height, bigScroll.position.z), "speed", 500f, "EaseType", iTween.EaseType.easeInOutSine, "LoopType", iTween.LoopType.none));
    //    }
    //}

    //if (Vector2.Distance(bigScroll.position, new Vector2(bigScroll.position.x, (2300f / 2688f) * Screen.height)) <= 0.1f)
    //{
    //    if (imagesScrollRect.verticalNormalizedPosition >= 1f)
    //    {
    //        bigScroll.GetComponent<LeanDragTranslate>().enabled = true;
    //    }
    //    else
    //    {
    //        bigScroll.GetComponent<LeanDragTranslate>().enabled = false;
    //    }
    //}
    //else
    //{
    //    bigScroll.GetComponent<LeanDragTranslate>().enabled = true;
    //}
    //}

    void checkSwipe()
    {
        //Check if Vertical swipe
        if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
        {
            if (fingerDown.y - fingerUp.y > 0)//up swipe
            {
                iTween.MoveTo(bigScroll.gameObject, iTween.Hash("position", new Vector3(bigScroll.position.x, (2300f / 2688f) * Screen.height, bigScroll.position.z), "speed", 1000f, "EaseType", iTween.EaseType.easeInOutSine, "LoopType", iTween.LoopType.none));
            }
            else if (fingerDown.y - fingerUp.y < 0)//Down swipe
            {
                if (imagesScrollRect.verticalNormalizedPosition >= 0.99f)
                    iTween.MoveTo(bigScroll.gameObject, iTween.Hash("position", new Vector3(bigScroll.position.x, (1350f / 2688f) * Screen.height, bigScroll.position.z), "speed", 1000f, "EaseType", iTween.EaseType.easeInOutSine, "LoopType", iTween.LoopType.none));
            }
            fingerUp = fingerDown;
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }

    public void onContinueRestartClick(int selection)
    {
        if (selection == 1)
        {
            // Continue
            StartCoroutine(loadScene(true));
        }
        else
        {
            string largePath = GameConfig.dataPath() + "/Back/" + GameConfig.folderName + "_" + GameConfig.regionNumber + ".png";
            string overlayPath = GameConfig.dataPath() + "/Overlay/" + GameConfig.folderName + "_" + GameConfig.regionNumber + ".png";
            string smallPath = GameConfig.regionIconPath() + "/" + GameConfig.regionNumber + ".png";
            string workSmallPath = GameConfig.dataPath() + "/Icon/" + GameConfig.folderName + "_" + GameConfig.regionNumber + ".png";
            //Delete
            if (File.Exists(largePath))
            {
                File.Delete(largePath);
            }
            if (File.Exists(overlayPath))
            {
                File.Delete(overlayPath);
            }
            if (File.Exists(smallPath))
            {
                File.Delete(smallPath);
            }
            if (File.Exists(workSmallPath))
            {
                File.Delete(workSmallPath);
            }
            GameConfig.setRegionProgress(GameConfig.folderName + GameConfig.regionNumber + "isEdited", 0);
            if (GameConfig.isWorks)
            {
                GameConfig.workIcons.RemoveAt(int.Parse(selectedUI.name) - 1);
                if (GameConfig.folderName == category_names[GameConfig.categoryIndex])
                {
                    GameConfig.regionIcons[GameConfig.regionNumber - 1] = Resources.Load(GameConfig.folderName + "/Small/" + GameConfig.regionNumber, typeof(Texture2D)) as Texture2D;
                }
                _myDemo.LoadLargeData();
            }
            else
            {
                loadOrReplaceImage(GameConfig.regionNumber, true);
                string worksName = GameConfig.folderName + "_" + GameConfig.regionNumber + ".png";
                bool found = false;
                int index = 0;
                for (; index < GameConfig.workIcons.Count; index++)
                {
                    if (GameConfig.workIcons[index].name == worksName)
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                {
                    GameConfig.workIcons.RemoveAt(index);
                }
            }
            onChoiceCloseClick();
        }
    }

    public void onChoiceCloseClick()
    {
        if (clickEnabled)
        {
            if (Vector3.Distance(unlockPanel.transform.position, overlay.transform.position) <= 0.01f)
            {
                animatePanel(unlockPanel, inAppPanelPos);
            }
            else if (Vector3.Distance(continuePanel.transform.position, positionTaker.position) <= 0.01f)
            {
                animatePanel(continuePanel, continuePanelPos);
            }
            showHideUi(overlay);
        }
    }

    public void movePanel(bool isIn, GameObject targetObj)
    {
        Vector3 target = isIn ? overlay.transform.position : inAppPanelPos;
        iTween.MoveTo(targetObj, iTween.Hash("position", target, "time", 0.5f));
    }

    public void showInAppPanel(bool status)
    {
        movePanel(status, inAppPanel);
    }

    public void onPurchaseUnlockAllPictures()
    {
        GameConfig.SetUnlockAllPictures();
        movePanel(false, inAppPanel);
        adjustUIPremium();
        //updateScroller();
        _myDemo.LoadLargeData();
    }

    public void onGetWeekPremium()
    {
        movePanel(false, inAppPanel);
        adjustUIPremium();
        //updateScroller();
        GameConfig.SetWeekSubscription();
        _myDemo.LoadLargeData();
        //AdsMediation.instance.hideBanner();
    }

    public void onWatchVideo()
    {


    }

    void videoCallback(bool isCompleted)
    {
        if (isCompleted)
        {
            animatePanel(unlockPanel, inAppPanelPos);
            GameConfig.setRegionProgress(GameConfig.folderName + GameConfig.regionNumber + "isUnlocked");
            StartCoroutine(loadScene(false));
        }
        else
        {
            StartCoroutine(showToast("Watch  Complete  Video"));
        }
    }

    void adjustUIPremium()
    {
        jugar.sizeDelta = new Vector2(jugar.sizeDelta.x, 900f);
        freeTrialBar.SetActive(false);
        imagesScrollRect.GetComponent<RectTransform>().offsetMax = new Vector2(0f, -620);
        ispremium =true;
    }
    public void adjustUINormal()
    {
        jugar.sizeDelta = new Vector2(jugar.sizeDelta.x, 1000f);
        freeTrialBar.SetActive(true);
        imagesScrollRect.GetComponent<RectTransform>().offsetMax = new Vector2(0f, -420);
        ispremium =false;
    }

    void loadImages()
    {
        if (GameConfig.regionIcons != null)
        {
            GameConfig.regionIcons.Clear();
        }
        Resources.UnloadUnusedAssets();
        GameConfig.regionIcons = new List<Texture2D>();
        for (var i = 0; i < images_count[GameConfig.categoryIndex]; i++)
        {
            loadOrReplaceImage(i + 1);
        }
        _myDemo.LoadLargeData();
    }

    void loadOrReplaceImage(int index, bool isReplace = false)
    {
        string prgPath = GameConfig.regionIconPath() + "/" + index + ".png";
        Texture2D loadedTex = new Texture2D(512, 512, TextureFormat.RGBA32, false);
        if (File.Exists(prgPath))
        {
            loadedTex.LoadImage(File.ReadAllBytes(prgPath));
        }
        else
        {
            loadedTex = Resources.Load(category_names[GameConfig.categoryIndex] + "/Small/" + index, typeof(Texture2D)) as Texture2D;
        }
        if (isReplace)
        {
            GameConfig.regionIcons[index - 1] = null;
            Resources.UnloadUnusedAssets();
            GameConfig.regionIcons[index - 1] = loadedTex;
            selectedUI.transform.GetChild(0).GetComponent<RawImage>().texture = loadedTex;
        }
        else
        {
            GameConfig.regionIcons.Add(loadedTex);
        }
    }

    void loadCategories()
    {
        for (var i = 0; i < category_names.Count; i++)
        {
            var categoreyButton = Instantiate(categoreyPrefab, categoriesContent);
            categoreyButton.name = category_names[i];
            categoreyButton.transform.GetChild(0).GetComponent<Text>().text = categoreyButton.name;
            categoreyButton.GetComponent<Button>().onClick.AddListener(onCategoreyClick);
            if (i == GameConfig.categoryIndex)
            {
                categoreyButton.transform.GetChild(1).gameObject.SetActive(true);
                categoreyButton.transform.GetChild(0).GetComponent<Text>().color = Color.black;
            }
        }
    }

    public void onRegionButtonClick()
    {
        GameConfig.imagesScrollVal = imagesScrollRect.verticalNormalizedPosition;
        GameConfig.categoriesScrollVal = categoriesContent.parent.GetComponent<ScrollRect>().horizontalNormalizedPosition;
        selectedUI = EventSystem.current.currentSelectedGameObject;
        clickEnabled = false;
        if (GameConfig.isWorks)
        {
            string texName = selectedUI.transform.GetChild(0).GetComponent<RawImage>().texture.name;
            var nameStr = texName.Split('.');
            var nameSubStr = nameStr[0].Split('_');
            GameConfig.folderName = nameSubStr[0];
            GameConfig.regionNumber = int.Parse(nameSubStr[1]);
            GameConfig.workCatIndex = category_names.IndexOf(GameConfig.folderName);
        }
        else
        {
            GameConfig.selectedCell = int.Parse(selectedUI.transform.parent.name);
            GameConfig.regionNumber = int.Parse(selectedUI.name);
        }
        if (selectedUI.transform.GetChild(0).GetChild(1).gameObject.activeSelf)
        {
            showInAppPanel(true);
        }
        else
        {
            if (GameConfig.getRegionProgress(GameConfig.folderName + GameConfig.regionNumber + "isEdited") == 1)
            {
                animatePanel(continuePanel, positionTaker.position);
                showHideUi(overlay);
            }
            else
            {
                MMVibrationManager.Haptic(HapticTypes.MediumImpact);
                StartCoroutine(loadScene(true));
            }
        }
    }

    void onCategoreyClick()
    {
        var selectedCategorey = EventSystem.current.currentSelectedGameObject;
        if (selectedCategorey.transform.GetSiblingIndex() != GameConfig.categoryIndex)
        {
            imagesScrollRect.verticalNormalizedPosition = 1f;
            categoriesContent.GetChild(GameConfig.categoryIndex).GetChild(1).gameObject.SetActive(false);
            categoriesContent.GetChild(GameConfig.categoryIndex).GetChild(0).GetComponent<Text>().color = selectedColor;
            selectedCategorey.transform.GetChild(1).gameObject.SetActive(true);
            selectedCategorey.transform.GetChild(0).GetComponent<Text>().color = Color.black;
            GameConfig.categoryIndex = selectedCategorey.transform.GetSiblingIndex();
            GameConfig.folderName = category_names[GameConfig.categoryIndex];
            loadImages();
        }
    }

    void showHideUi(GameObject current)
    {
        current.SetActive(!current.activeSelf);
    }

    IEnumerator loadScene(bool showAd)
    {
        loadProgress.fillAmount = 0f;
        showHideUi(loadingPanel);
        AsyncOperation operation = SceneManager.LoadSceneAsync("Painter");
        while (!operation.isDone)
        {
            loadProgress.fillAmount = Mathf.Clamp01(operation.progress / 0.9f);
            yield return null;
        }
    }

    void animatePanel(GameObject target, Vector3 targetPos)
    {
        iTween.MoveTo(target, iTween.Hash("x", targetPos.x, "y", targetPos.y, "time", 0.1f, "oncomplete", "completeCallback", "oncompletetarget", gameObject));
    }

    void completeCallback()
    {
        clickEnabled = true;
    }

    IEnumerator progressLoading()
    {
        yield return new WaitForSeconds(1f);
        showHideUi(loadingPanel);
    }

    IEnumerator showToast(string message)
    {
        yield return new WaitForEndOfFrame();
        toast.transform.GetChild(0).GetComponent<Text>().text = message;
        toast.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        toast.SetActive(false);
    }

    public void RequestForPremium()
    {
        Purchaser.instance.SubscribeToPremium();
    }

    public void DownloadLater()
    {
        InternetPanel.SetActive(false);
    }

    public void showInternetPanel()
    {
        InternetPanel.SetActive(true);
    }

    public void soundOnOff()
    {
        var selected = EventSystem.current.currentSelectedGameObject;
        selected.GetComponent<Image>().sprite = sound[status];
        status = (status == 1 ? 0 : 1);
    }

    public void showSettings(bool status)
    {
        movePanel(status, settingsPanel);
    }

    public void onTabClick(int tabIndex)
    {
        if (selectedTab != tabIndex)
        {
            switch (tabIndex)
            {
                case 0:
                    GameConfig.isWorks = false;
                    GameConfig.workCatIndex = -1;
                    //  scrollerContent.offsetMax = new Vector2(0f, -703f);
                     if (ispremium){
                    jugar.sizeDelta = new Vector2(jugar.sizeDelta.x, 900f);
        freeTrialBar.SetActive(false);
        imagesScrollRect.GetComponent<RectTransform>().offsetMax = new Vector2(0f, -620);
                     }else{
                        jugar.sizeDelta = new Vector2(jugar.sizeDelta.x, 1000f);
        freeTrialBar.SetActive(true);
        imagesScrollRect.GetComponent<RectTransform>().offsetMax = new Vector2(0f, -720);
                     }
                    categoriesContent.parent.gameObject.SetActive(true);
                    
                    GameConfig.folderName = category_names[GameConfig.categoryIndex];
                    _myDemo.LoadLargeData();
                    StartCoroutine(goToCell(false));
                    selectedTab = tabIndex;
                    break;
                case 1:
                    StartCoroutine(showToast("Coming Soon!"));
                    break;
                case 2:
                    Application.OpenURL("https://www.google.com/");
                    break;
                case 3:
                    showWorks(tabIndex);
                    break;
            }
        }
    }

    void showWorks(int tabIndex)
    {
        if (GameConfig.workIcons.Count == 0)
        {
            string worksPath = GameConfig.dataPath() + "/Icon";
            if (Directory.Exists(worksPath))
            {
                DirectoryInfo dir = new DirectoryInfo(worksPath);
                FileInfo[] files = dir.GetFiles("*.png").OrderByDescending(p => p.CreationTime).ToArray();
                if (files.Length > 0)
                {
                    for (var i = 0; i < files.Length; i++)
                    {
                        Texture2D loadedTex = new Texture2D(512, 512, TextureFormat.RGBA32, false);
                        loadedTex.LoadImage(File.ReadAllBytes(files[i].FullName));
                        loadedTex.name = files[i].Name;
                        GameConfig.workIcons.Add(loadedTex);
                    }
                }
                else
                {
                    StartCoroutine(showToast("You not have any works yet."));
                }
            }
            else
            {
                StartCoroutine(showToast("You not have any works yet."));
            }
        }
        else
        {
            scrollerContent.offsetMax = new Vector2(0f, -503f);
            jugar.sizeDelta = new Vector2(jugar.sizeDelta.x, 800f);
            freeTrialBar.SetActive(false);
            categoriesContent.parent.gameObject.SetActive(false);
            GameConfig.isWorks = true;
            selectedTab = tabIndex;
            _myDemo.LoadLargeData();
        }
    }

    public void onRateClick()
    {
        // Device.RequestStoreReview();
    }

    public void onInstaClick()
    {
        Application.OpenURL("https://www.instagram.com/davincicoloring");
    }

    public void onEbookClick()
    {
        Application.OpenURL("https://bit.ly/FREEAirjordanbook");
    }

    public void onBookstoreClick()
    {
        Application.OpenURL("http://www.coloringbooklife.com");
    }
}
