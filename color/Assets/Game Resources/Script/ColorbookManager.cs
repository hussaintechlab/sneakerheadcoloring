﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

    public class ColorbookManager : MonoBehaviour
    {
        public GameObject BrushPanel, WheelPanel;
        bool panelStatus = false;
        public Image BrushSelectedImg;
        public Sprite[] BrushSprite;
        public static ColorbookManager instance;
        private void Start()
        {
            instance = this;
          //  BrushPanelOff();
         //   ClickOnBrush(PlayerPrefs.GetInt("brush"));
            
        }
        public void BrushPanelOn()
        {
            panelStatus = !panelStatus;
            BrushPanel.SetActive(panelStatus);
        }
        public void ClickOnBrush(int num)
        {
            BrushSelectedImg.sprite = BrushSprite[num];
            PlayerPrefs.SetInt("brush", num);
        }
        public void BrushPanelOff()
        {
            panelStatus = false;
            BrushPanel.SetActive(false);
        }
        public void WheelPanelOn()
        {
            WheelPanel.SetActive(true);
        }
        public void WheelPanelOff()
        {
            WheelPanel.SetActive(false);
        }

    
}