﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;

public class SteeringWheel : MonoBehaviour
{
    public Graphic UI_Element;

    RectTransform rectT;
    Vector2 centerPoint;
    Vector3 preAngle;
    public float[] colorAngleList;
    public ColorWheel WheelParentObject;
    float wheelReleasedSpeed = 0;
    float wheelAngle = 0f;
    float wheelPrevAngle = 0f;

    bool wheelBeingHeld = false;
    UnityEngine.Audio.AudioMixerGroup pitchBendGroup;

    public float GetClampedValue()
    {
        // returns a value in range [-1,1] similar to GetAxis("Horizontal")
        return wheelAngle;/// maximumSteeringAngle;
    }

    public float GetAngle()
    {
        // returns the wheel angle itself without clamp operation
        return wheelAngle;
    }

    void Start()
    {
        rectT = UI_Element.rectTransform;
        InitEventsSystem();
        //pitchBendGroup = Resources.Load<UnityEngine.Audio.AudioMixerGroup>("Pitch Bend Mixer");
        //WheelManager.instance._sound.outputAudioMixerGroup = pitchBendGroup;
    }

    void Update()
    {
        // If the wheel is released, reset the rotation
        // to initial (zero) rotation by wheelReleasedSpeed degrees per second
        if (!wheelBeingHeld && !Mathf.Approximately(0f, wheelAngle))
        {
            float deltaAngle = wheelReleasedSpeed * Time.deltaTime;
            if (Mathf.Abs(deltaAngle) > Mathf.Abs(wheelAngle))
                wheelAngle = 0f;
            else if (wheelAngle > 0f)
                wheelAngle -= deltaAngle;
            else
                wheelAngle += deltaAngle;
        }
        if (wheelBeingHeld)
        {
            // Rotate the wheel image
            preAngle = rectT.localEulerAngles;
            rectT.localEulerAngles = Vector3.back * wheelAngle * 1.25f;
            if (Mathf.Approximately(Vector3.Distance(preAngle, rectT.localEulerAngles), 0f))
            {
               // WheelManager.instance._sound.Pause();
            }
        }
    }

    void InitEventsSystem()
    {
        // Warning: Be ready to see some extremely boring code here :-/
        // You are warned!
        EventTrigger events = UI_Element.gameObject.GetComponent<EventTrigger>();

        if (events == null)
            events = UI_Element.gameObject.AddComponent<EventTrigger>();

        if (events.triggers == null)
            events.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();

        EventTrigger.Entry entry = new EventTrigger.Entry();
        EventTrigger.TriggerEvent callback = new EventTrigger.TriggerEvent();
        UnityAction<BaseEventData> functionCall = new UnityAction<BaseEventData>(PressEvent);
        callback.AddListener(functionCall);
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback = callback;

        events.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        callback = new EventTrigger.TriggerEvent();
        functionCall = new UnityAction<BaseEventData>(DragEvent);
        callback.AddListener(functionCall);
        entry.eventID = EventTriggerType.Drag;
        entry.callback = callback;
        events.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        callback = new EventTrigger.TriggerEvent();
        functionCall = new UnityAction<BaseEventData>(DragEndEvent);
        callback.AddListener(functionCall);
        entry.eventID = EventTriggerType.EndDrag;
        entry.callback = callback;
        events.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        callback = new EventTrigger.TriggerEvent();
        functionCall = new UnityAction<BaseEventData>(ReleaseEvent);//
        callback.AddListener(functionCall);
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback = callback;

        events.triggers.Add(entry);
    }

    public void PressEvent(BaseEventData eventData)
    {
        // Executed when mouse/finger starts touching the steering wheel
        Vector2 pointerPos = ((PointerEventData)eventData).position;

        wheelBeingHeld = true;
        centerPoint = RectTransformUtility.WorldToScreenPoint(((PointerEventData)eventData).pressEventCamera, rectT.position);
        wheelPrevAngle = Vector2.Angle(Vector2.up, pointerPos - centerPoint);
    }

    public void DragEvent(BaseEventData eventData)
    {
        // Executed when mouse/finger is dragged over the steering wheel
        Vector2 pointerPos = ((PointerEventData)eventData).position;

        float wheelNewAngle = Vector2.Angle(Vector2.up, pointerPos - centerPoint);
        // Do nothing if the pointer is too close to the center of the wheel
        if (Vector2.Distance(pointerPos, centerPoint) > 20f)
        {
            if (pointerPos.x > centerPoint.x)
                wheelAngle += wheelNewAngle - wheelPrevAngle;
            else
                wheelAngle -= wheelNewAngle - wheelPrevAngle;

            if (!WheelManager.instance._sound.isPlaying)
            {
             //   WheelManager.instance._sound.Play();
            }
            float rotaDiff = Mathf.Clamp(Mathf.Abs(wheelNewAngle - wheelPrevAngle), 1f, 20f);
           // WheelManager.instance._sound.pitch = rotaDiff;
            //pitchBendGroup.audioMixer.SetFloat("pitchBend", 1f / rotaDiff);
        }
        wheelPrevAngle = wheelNewAngle;
    }

    public void DragEndEvent(BaseEventData eventData)
    {
       // WheelManager.instance._sound.Pause();
    }

    public void ReleaseEvent(BaseEventData eventData)
    {
        // Executed when mouse/finger stops touching the steering wheel
        // Performs one last DragEvent, just in case
        DragEvent(eventData);

        wheelBeingHeld = false;
        float zRotation = GetComponent<RectTransform>().eulerAngles.z;
        // Debug.Log("Input Value : " + zRotation);
        //  Debug.Log("Output Value : " + NearestIndex(zRotation));
        rectT.localEulerAngles = new Vector3(0, 0, NearestIndex(zRotation));
    }

    float NearestIndex(float value)
    {
        float minmum = Mathf.Abs(colorAngleList[0] - value);
        int index = 0;
        for (int i = 0; i < colorAngleList.Length; i++)
        {
            if (minmum > Mathf.Abs(colorAngleList[i] - value))
            {
                minmum = Mathf.Abs(colorAngleList[i] - value);
                index = i;

            }

            if (minmum > Mathf.Abs(value - colorAngleList[i]))
            {
                minmum = Mathf.Abs(colorAngleList[i] - value);
                index = i;

            }
        }
        minmum = colorAngleList[index];
        WheelParentObject.ColorImg.color = WheelParentObject.wheelcolorList[index];
        Painting.instance.setPaintColor(WheelParentObject.wheelcolorList[index]);
        return minmum;
    }
}