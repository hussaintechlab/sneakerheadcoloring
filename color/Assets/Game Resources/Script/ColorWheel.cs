﻿
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ColorWheel : MonoBehaviour
{

    public float[] colorAngleList;
    public Vector3 directionVector;
    public float baseAngle;
    public bool currentStatus;
    public WheelSprite spritScript;
    public GameObject fadeImage;
    public Image ColorImg, WheelImg, gredientImg, boundryImg;
    public GameObject clicker;
    public Text NameText;
    // public Color currentColor;
    public List<Color> wheelcolorList;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        if (currentStatus == true)
        {
            Invoke("colorSet", 1f);
        }
    }
    public void WheelSetup(bool isactive)
    {
        wheelcolorList = spritScript.wheelColorList;
        WheelImg.sprite = spritScript.wheelsprite;
        WheelImg.rectTransform.localRotation = Quaternion.identity;
        NameText.text = spritScript.wheelName;
        if (clicker.GetComponent<iTween>() != null)
        {
            DestroyImmediate(clicker.GetComponent<iTween>());
            clicker.transform.localScale = Vector3.one;
        }
        StopAllCoroutines();

        if (isactive)
        {
            //fadeImage.SetActive(false);
            ColorImg.gameObject.SetActive(true);
            currentStatus = true;
            //  if (ColorImg.color == Color.white)
            {
                Painting.instance.setPaintColor(wheelcolorList[0]);
                ColorImg.color = wheelcolorList[0];
            }

            boundryImg.gameObject.SetActive(true);
            StartCoroutine(playScale());
        }
        else
        {
            //fadeImage.SetActive(true);
            ColorImg.gameObject.SetActive(false);
            currentStatus = false;
            boundryImg.gameObject.SetActive(false);
        }


    }
    void colorSet()
    {
        Painting.instance.setPaintColor(ColorImg.color);
    }

    IEnumerator playScale()
    {
        yield return new WaitForSeconds(4f);
        iTween.ScaleTo(clicker, iTween.Hash("scale", Vector3.one * 1.2f, "time", 0.5f));
        yield return new WaitForSeconds(1f);
        iTween.ScaleTo(clicker, iTween.Hash("scale", Vector3.one, "time", 0.5f));
        StartCoroutine(playScale());
    }
}
