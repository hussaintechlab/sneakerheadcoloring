﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GG.Infrastructure.Utils.Swipe;
public class WheelManager : MonoBehaviour
{
    public List<WheelSprite> AllWheelList;
    public List<WheelObject> AllWheelObjectList;
    public List<RectTransform> wheelsPostionList;
    public static WheelManager instance;
    public int currentWheelIndex, spritIndex;
    int left, right, left2, right2;
    bool clickDelay, isLeftCLick, isRightClick;
    public AudioSource _sound;
    public int lockedWheels;
public Text numberText;
    private void Awake()
    {
        instance = this;
        currentWheelIndex = PlayerPrefs.GetInt("currentWheel");
        spritIndex = PlayerPrefs.GetInt("spritIndex");
        //  OnStart();
    }

    private void Start()
    {
        WhellSpriteUpdate(.001f);
    }

    void WheelRightRotate()
    {
        WheelObject temp = AllWheelObjectList[0];
        for (int i = 0; i < AllWheelObjectList.Count; i++)
        {
            if (i + 1 < AllWheelObjectList.Count)
            {
                AllWheelObjectList[i] = AllWheelObjectList[i + 1];
            }
            else
            {
                AllWheelObjectList[i] = temp;
            }
        }
        numberText.gameObject.SetActive(false);
        WhellSpriteUpdate(0.5f);
        WheelSetPositions(true);
    }

    void WheelLeftRotate()
    {
        WheelObject temp = AllWheelObjectList[AllWheelObjectList.Count - 1];
        for (int i = AllWheelObjectList.Count - 1; i >= 0; i--)
        {
            if (i - 1 >= 0)
            {
                AllWheelObjectList[i] = AllWheelObjectList[i - 1];
            }
            else
            {
                AllWheelObjectList[i] = temp;
            }
        }
        numberText.gameObject.SetActive(false);
        WhellSpriteUpdate(0.5f);
        WheelSetPositions(false);
    }
    void WheelSetPositions(bool isRight)
    {
        for (int i = 0; i < AllWheelObjectList.Count; i++)
        {
            // For Wheel Position Update
            if (i == 0 && !isRight)
            {
                AllWheelObjectList[i].WheelScript.transform.position = wheelsPostionList[i].position;
            }
            else if (i == 4 && isRight)
            {
                AllWheelObjectList[i].WheelScript.transform.position = wheelsPostionList[i].position;
            }
            else
            {
                iTween.MoveTo(AllWheelObjectList[i].WheelScript.gameObject, iTween.Hash("position", wheelsPostionList[i].position, "time", .6f, "EaseType", iTween.EaseType.easeInOutSine, "LoopType", iTween.LoopType.none));
            }



            // For Wheel Size Update
          
                if (i == 2)
            {
                iTween.ScaleTo(AllWheelObjectList[i].WheelScript.gameObject, iTween.Hash("scale", new Vector3(1.2f, 1.2f, 1.2f), "time", .6f, "EaseType", iTween.EaseType.easeInOutSine, "LoopType", iTween.LoopType.none));
            }
            else
            {
                AllWheelObjectList[i].WheelScript.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                //  iTween.ScaleTo(AllWheelObjectList[i].WheelScript.gameObject, iTween.Hash("scale", new Vector3(0.5f, 0.5f, 0.5f), "speed", 1f, "EaseType", iTween.EaseType.easeInOutSine, "LoopType", iTween.LoopType.none));
            }
            
        }
    }
 void wheelnumber(){
     
      numberText.text=""+(1+spritIndex);
      numberText.gameObject.SetActive(true);
 }
  public  void WhellSpriteUpdate(float time)
    {
      Invoke("wheelnumber",time);
        // AllWheelObjectList[2].WheelScript.WheelImg.sprite = AllWheelList[spritIndex].wheelsprite;
        if (spritIndex - 1 < 0)
        {
            left = AllWheelList.Count - 1;
        }
        else
        {
            left = spritIndex - 1;
        }

        if (left - 1 < 0)
        {
            left2 = AllWheelList.Count - 1;
        }
        else
        {
            left2 = left - 1;
        }

        if (spritIndex + 1 >= AllWheelList.Count)
        {
            right = 0;
        }
        else
        {
            right = spritIndex + 1;
        }
        if (right + 1 >= AllWheelList.Count)
        {
            right2 = 0;
        }
        else
        {
            right2 = right + 1;
        }
        Debug.Log("Left 2 : " + left2);

        AllWheelObjectList[0].wheelobj = AllWheelList[left2];
        AllWheelObjectList[0].WheelScript.spritScript = AllWheelList[left2];
        AllWheelObjectList[0].WheelScript.WheelSetup(false);

        AllWheelObjectList[1].wheelobj = AllWheelList[left];
        AllWheelObjectList[1].WheelScript.spritScript = AllWheelList[left];
        AllWheelObjectList[1].WheelScript.WheelSetup(false);

        AllWheelObjectList[2].wheelobj = AllWheelList[spritIndex];
        AllWheelObjectList[2].WheelScript.spritScript = AllWheelList[spritIndex];
        AllWheelObjectList[2].WheelScript.WheelSetup(true);

        AllWheelObjectList[3].wheelobj = AllWheelList[right];
        AllWheelObjectList[3].WheelScript.spritScript = AllWheelList[right];
        AllWheelObjectList[3].WheelScript.WheelSetup(false);

        AllWheelObjectList[4].wheelobj = AllWheelList[right2];
        AllWheelObjectList[4].WheelScript.spritScript = AllWheelList[right2];
        AllWheelObjectList[4].WheelScript.WheelSetup(false);


    }
    public void WheelRefresh()
    {
        
        if (spritIndex - 1 < 0)
        {
            left = AllWheelList.Count - 1;
        }
        else
        {
            left = spritIndex - 1;
        }

        if (left - 1 < 0)
        {
            left2 = AllWheelList.Count - 1;
        }
        else
        {
            left2 = left - 1;
        }

        if (spritIndex + 1 >= AllWheelList.Count)
        {
            right = 0;
        }
        else
        {
            right = spritIndex + 1;
        }
        if (right + 1 >= AllWheelList.Count)
        {
            right2 = 0;
        }
        else
        {
            right2 = right + 1;
        }
        AllWheelObjectList[0].wheelobj = AllWheelList[left2];
        AllWheelObjectList[0].WheelScript.spritScript = AllWheelList[left2];
        AllWheelObjectList[0].WheelScript.WheelSetup(false);

        AllWheelObjectList[1].wheelobj = AllWheelList[left];
        AllWheelObjectList[1].WheelScript.spritScript = AllWheelList[left];
        AllWheelObjectList[1].WheelScript.WheelSetup(false);

        AllWheelObjectList[2].wheelobj = AllWheelList[spritIndex];
        AllWheelObjectList[2].WheelScript.spritScript = AllWheelList[spritIndex];
        AllWheelObjectList[2].WheelScript.WheelSetup(true);

        AllWheelObjectList[3].wheelobj = AllWheelList[right];
        AllWheelObjectList[3].WheelScript.spritScript = AllWheelList[right];
        AllWheelObjectList[3].WheelScript.WheelSetup(false);

        AllWheelObjectList[4].wheelobj = AllWheelList[right2];
        AllWheelObjectList[4].WheelScript.spritScript = AllWheelList[right2];
        AllWheelObjectList[4].WheelScript.WheelSetup(false);
    }
    //void OnStart()
    //{
    //    //left
    //    if (currentWheelIndex - 1 < 0)
    //    { left = AllWheelList.Count - 1; }
    //    else
    //    { left = currentWheelIndex - 1; }

    //    //left 2
    //    if (currentWheelIndex - 2 < 0)
    //    { left2 = AllWheelList.Count - 2; }
    //    else
    //    { left2 = currentWheelIndex - 2; }

    //    //right
    //    if (currentWheelIndex +1 >= AllWheelList.Count)
    //    { right = AllWheelList.Count + 1; }
    //    else
    //    { right = currentWheelIndex + 1; }

    //    //right2
    //    if (currentWheelIndex + 2 >= AllWheelList.Count)
    //    { right2 = (currentWheelIndex + 2)%5; }
    //    else
    //    { right2 = currentWheelIndex +2; }


    //    //For Current
    //    AllWheelObjectList[2].wheelobj = AllWheelList[currentWheelIndex];
    //    AllWheelObjectList[2].WheelSize = new Vector3(1, 1, 1);
    //    AllWheelObjectList[2].WheelScript.spritScript= AllWheelList[currentWheelIndex];
    //    AllWheelObjectList[2].WheelScript.currentStatus = true;
    //    AllWheelObjectList[2].currentStatus = true;
    //    AllWheelObjectList[2].wheelIndex = 2;

    //    //For Left

    //    AllWheelObjectList[1].wheelobj = AllWheelList[left];
    //    AllWheelObjectList[1].WheelSize = new Vector3(.5f, .5f, .5f);
    //    AllWheelObjectList[1].WheelScript.spritScript = AllWheelList[left];
    //    AllWheelObjectList[2].WheelScript.currentStatus = true;
    //    AllWheelObjectList[1].currentStatus = false;
    //    AllWheelObjectList[1].wheelIndex = 1;

    //    //For Left 2
    //    AllWheelObjectList[0].wheelobj = AllWheelList[left2];
    //    AllWheelObjectList[0].WheelSize = new Vector3(.5f, .5f, .5f);
    //    AllWheelObjectList[0].WheelScript.spritScript = AllWheelList[left2];
    //    AllWheelObjectList[2].WheelScript.currentStatus = true;
    //    AllWheelObjectList[0].currentStatus = false;
    //    AllWheelObjectList[0].wheelIndex = 0;

    //    //For Right
    //    AllWheelObjectList[3].wheelobj = AllWheelList[right];
    //    AllWheelObjectList[3].WheelSize = new Vector3(.5f, .5f, .5f);
    //    AllWheelObjectList[3].WheelScript.spritScript = AllWheelList[right];
    //    AllWheelObjectList[2].WheelScript.currentStatus = true;
    //    AllWheelObjectList[3].currentStatus = false;
    //    AllWheelObjectList[3].wheelIndex = 3;

    //    //For Right 2
    //    AllWheelObjectList[4].wheelobj = AllWheelList[right2];
    //    AllWheelObjectList[4].WheelSize = new Vector3(.5f, .5f, .5f);
    //    AllWheelObjectList[4].WheelScript.spritScript = AllWheelList[right2];
    //    AllWheelObjectList[2].WheelScript.currentStatus = true;
    //    AllWheelObjectList[4].currentStatus = false;
    //    AllWheelObjectList[4].wheelIndex = 4;

    //    RefreshWheel();
    //}
    //public void RefreshWheel()
    //{
    //    for (int i = 0; i < wheelsPostionList.Count; i++)
    //    {
    //        AllWheelObjectList[i].WheelScript.wheelObj.GetComponent<Image>().sprite = AllWheelObjectList[i].wheelobj.wheelsprite;
    //        AllWheelObjectList[i].WheelScript.transform.position = wheelsPostionList[i].position;
    //        AllWheelObjectList[i].WheelScript.transform.localScale = AllWheelObjectList[i].WheelSize;
    //    }
    //}
    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (isRightClick)
            {
                WheelRightRotate();
                isRightClick = false;
            }
            else if (isLeftCLick)
            {
                WheelLeftRotate();
                isLeftCLick = false;
            }
        }
    }

    public void RightClick()
    {
        if (clickDelay)
        {
            return;
        }
        clickDelay = true;
        Invoke("ClickReset", .9f);
        Debug.Log("Right");
        spritIndex += 1;
        if (GameConfig.GetWeekSubscription() == 0)
        {
            if (spritIndex >= WheelManager.instance.lockedWheels + 1)
            {
                spritIndex = 0;
            }
        }
        else
        {
            if (spritIndex >= AllWheelList.Count)
            {
                spritIndex = 0;
            }
        }

        PlayerPrefs.SetInt("spritIndex", spritIndex);
        isRightClick = true;
        //   OnStart();
    }
    void ClickReset()
    {
        clickDelay = false;
    }
    public void LeftClick()
    {
        if (clickDelay)
        {
            return;
        }
        clickDelay = true;
        Invoke("ClickReset", .9f);

        Debug.Log("Left");
        spritIndex -= 1;

        if (GameConfig.GetWeekSubscription() == 0)
        {
            if (spritIndex < 0)
            {
                spritIndex = WheelManager.instance.lockedWheels + 1;
            }
        }
        else
        {
            if (spritIndex < 0)
            {
                spritIndex = AllWheelList.Count - 1;
            }
        }


        PlayerPrefs.SetInt("spritIndex", spritIndex);
        isLeftCLick = true;
        //OnStart();
    }
    public void OnSwipeHandler(string id)
    {
        //switch(id)
        //{
        //    case DirectionId.ID_LEFT:
        //        LeftClick();
        //        break;
        //    case DirectionId.ID_RIGHT:
        //        RightClick();
        //        break;

        //}
    }
    public void updateWheelsUi(bool isGredient)
    {
        for (var i = 0; i < AllWheelObjectList.Count; i++)
        {
            AllWheelObjectList[i].WheelScript.gredientImg.gameObject.SetActive(isGredient);
        }
    }

    //  public 
}
[System.Serializable]
public class WheelSprite
{
    public Sprite wheelsprite;
    public List<Color> wheelColorList;
    public string wheelName;
}
[System.Serializable]
public class WheelObject
{
    public WheelSprite wheelobj;
    public ColorWheel WheelScript;
    public Vector3 WheelSize;
    public int wheelIndex;
    public bool currentStatus;
}
