﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MyScrollIndexCallback : MonoBehaviour
{
    public Text text, wheelName;
    public Image gredient;
    public GameObject lockImg;
    int temp;

    // private static float[] randomWidths = new float[3] { 100, 150, 50 };
    void ScrollCellIndex(int idx)
    {
        temp = FindIndex(idx);
        string name = "Cell " + temp.ToString();
        if (text != null)
        {
            text.text = (temp + 1).ToString();
        }
        GetComponent<Image>().sprite = WheelManager.instance.AllWheelList[temp].wheelsprite;
        gameObject.name = name;
        wheelName.text = WheelManager.instance.AllWheelList[temp].wheelName;
        gredient.enabled = Painting.instance.isGredient;
        if (GameConfig.GetWeekSubscription() == 0 && temp > WheelManager.instance.lockedWheels)
        {
            lockImg.SetActive(true);
        }
        else
        {
            lockImg.SetActive(false);
        }
    }
    int FindIndex(int idx)
    {

        int temp;// = idx + WheelManager.instance.currentWheelIndex;
        temp = idx % WheelManager.instance.AllWheelList.Count;
        if (temp < 0)
        {
            temp = WheelManager.instance.AllWheelList.Count + temp;
        }
        return temp;
    }
    public void OnClick()
    {
        if (lockImg.activeSelf)
        {
            return;
        }
        PlayerPrefs.SetInt("spritIndex", temp);
        WheelManager.instance.spritIndex = temp;
        RegionsManager.instance.wheelPanelOff();
        WheelManager.instance.WheelRefresh();
        WheelManager.instance.WhellSpriteUpdate(.001f);
    }
}
