﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageColorChange : MonoBehaviour
{
    public Texture2D image;
    public Color color;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine( TextureColorChange(image,1,1, color));
       // image.Apply();
    }

    public struct Point
    {
        public short x;
        public short y;
        public Point(short aX, short aY) { x = aX; y = aY; }
        public Point(int aX, int aY) : this((short)aX, (short)aY) { }
    }
    IEnumerator TextureColorChange( Texture2D aTex, int aX, int aY, Color aFillColor)
    {
        Debug.Log("Coroutine Called");
        int w = aTex.width;
        int h = aTex.height;
        Color[] colors = aTex.GetPixels();
        Color refCol = colors[aX + aY * w];
        Queue<Point> nodes = new Queue<Point>();
        nodes.Enqueue(new Point(aX, aY));
        while (nodes.Count > 0)
        {
            Point current = nodes.Dequeue();
            for (int i = current.x; i < w; i++)
            {
                Color C = colors[i + current.y * w];
                if (C != refCol || C == aFillColor)
                    break;
                colors[i + current.y * w] = aFillColor;
                if (current.y + 1 < h)
                {
                    C = colors[i + current.y * w + w];
                    if (C == refCol && C != aFillColor)
                    {
                        nodes.Enqueue(new Point(i, current.y + 1));
                       // yield return new WaitForSeconds(.1f);
                    }
                        
                }
                if (current.y - 1 >= 0)
                {
                    C = colors[i + current.y * w - w];
                    if (C == refCol && C != aFillColor)
                    {
                        nodes.Enqueue(new Point(i, current.y - 1));
                        yield return new WaitForSeconds(.001f);
                        aTex.SetPixels(colors);
                        image.Apply();
                    }
                        
                }
            }
            for (int i = current.x - 1; i >= 0; i--)
            {
                Color C = colors[i + current.y * w];
                if (C != refCol || C == aFillColor)
                    break;
                colors[i + current.y * w] = aFillColor;
                if (current.y + 1 < h)
                {
                    C = colors[i + current.y * w + w];
                    if (C == refCol && C != aFillColor)
                    {
                        nodes.Enqueue(new Point(i, current.y + 1));
                        //yield return new WaitForSeconds(.1f);
                    }
                        
                }
                if (current.y - 1 >= 0)
                {
                    C = colors[i + current.y * w - w];
                    if (C == refCol && C != aFillColor)
                    {
                        Debug.Log("4");
                        nodes.Enqueue(new Point(i, current.y - 1));
                        //yield return new WaitForSeconds(.1f);
                    }
                        

                }
            }
        }
        aTex.SetPixels(colors);
        yield return new WaitForSeconds(.1f);
        image.Apply();
    }
}
