﻿using UnityEngine;
using System.Collections;
using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using System;

namespace EnhancedScrollerDemos.SuperSimpleDemo
{
    public class SimpleDemo : MonoBehaviour, IEnhancedScrollerDelegate
    {

        private SmallList<Data> _data;

        public EnhancedScroller scroller;

        public EnhancedScrollerCellView cellViewPrefab;

        void Start()
        {
            scroller.Delegate = this;
        }

        public void LoadLargeData()
        {
            scroller.ReloadData();
        }

        private void LoadSmallData()
        {
            // set up some simple data
            _data = new SmallList<Data>();

            _data.Add(new Data() { someText = "A" });
            _data.Add(new Data() { someText = "B" });
            _data.Add(new Data() { someText = "C" });

            // tell the scroller to reload now that we have the data
            scroller.ReloadData();
        }

        #region UI Handlers

        public void LoadLargeDataButton_OnClick()
        {
            LoadLargeData();
        }

        public void LoadSmallDataButton_OnClick()
        {
            LoadSmallData();
        }

        #endregion

        #region EnhancedScroller Handlers

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            if (GameConfig.isWorks)
            {
                return Mathf.CeilToInt(GameConfig.workIcons.Count / 2f);
            }
            else
            {
                return Mathf.CeilToInt(GameConfig.regionIcons.Count / 2f);
            }
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            // in this example, even numbered cells are 30 pixels tall, odd numbered cells are 100 pixels tall
            return 380f;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            CellView cellView = scroller.GetCellView(cellViewPrefab) as CellView;
            cellView.name = dataIndex.ToString();
            setData(cellView, cellIndex);
            return cellView;
        }

        int newindex = 0;

        void setData(CellView cell, int index)
        {
            newindex = (index * 2);
            newindex++;
            cell.cell1.name = newindex + "";
            cell.cell1.GetComponent<Button>().onClick.RemoveAllListeners();
            int dataLength = GameConfig.isWorks ? GameConfig.workIcons.Count : GameConfig.regionIcons.Count;
            if (newindex < dataLength + 1)
            {
                cell.cell1.transform.GetChild(0).gameObject.GetComponent<RawImage>().texture = GameConfig.isWorks ? GameConfig.workIcons[newindex - 1] : GameConfig.regionIcons[newindex - 1];
                cell.cell1.GetComponent<Button>().onClick.AddListener(() =>
                {
                    MainUiHandler.Instance.onRegionButtonClick();
                });
                if (GameConfig.isWorks)
                {
                    cell.cell1.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                }
                else
                {
                    if (newindex % 3 == 0 && GameConfig.getRegionProgress(GameConfig.folderName + newindex + "isUnlocked") == 0 && GameConfig.GetWeekSubscription() == 0)
                    {
                        cell.cell1.transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
                    }
                    else
                    {
                        cell.cell1.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                    }
                }

                cell.cell1.SetActive(true);
            }
            else
            {
                cell.cell1.SetActive(false);
            }


            newindex++;
            cell.cell2.name = newindex + "";
            cell.cell2.GetComponent<Button>().onClick.RemoveAllListeners();

            if (newindex < dataLength + 1)
            {
                cell.cell2.transform.GetChild(0).gameObject.GetComponent<RawImage>().texture = GameConfig.isWorks ? GameConfig.workIcons[newindex - 1] : GameConfig.regionIcons[newindex - 1];
                cell.cell2.GetComponent<Button>().onClick.AddListener(() =>
                {
                    MainUiHandler.Instance.onRegionButtonClick();
                });
                if (GameConfig.isWorks)
                {
                    cell.cell2.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                }
                else
                {
                    if (newindex % 3 == 0 && GameConfig.getRegionProgress(GameConfig.folderName + newindex + "isUnlocked") == 0 && GameConfig.GetWeekSubscription() == 0)
                    {
                        cell.cell2.transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
                    }
                    else
                    {
                        cell.cell2.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                    }
                }
                cell.cell2.SetActive(true);
            }
            else
            {
                cell.cell2.SetActive(false);
            }
        }
        #endregion
    }
}
