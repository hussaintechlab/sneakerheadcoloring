﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using EnhancedUI.EnhancedScroller;
using System.Collections;
using System.IO;


namespace EnhancedScrollerDemos.SuperSimpleDemo
{
	/// <summary>
	/// This is the view of our cell which handles how the cell looks.
	/// </summary>


	public class CellView : EnhancedScrollerCellView
	{
		public GameObject cell1;
		public GameObject cell2;

		public void SetData (Data data)
		{

		}
	}
}